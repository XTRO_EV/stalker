<?php
namespace STALKER_CMS\Core\Uploads\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;

/**
 * Class FileTypes
 * @package STALKER_CMS\Core\Uploads\Models
 */
class FileTypes extends BaseModel implements ModelInterface {

    protected $table = 'file_types';
    protected $fillable = ['title', 'icon', 'extensions', 'mime_type', 'enabled'];
    protected $guarded = [];

    public $timestamps = FALSE;

    public function insert($request) {
        // TODO: Implement insert() method.
    }

    public function replace($id, $request) {

        $model = static::findOrFail($id);
        $model->enabled = $request::input('enabled');
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        return static::findOrFail($id)->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getTitleAttribute() {

        if (!empty($this->attributes['title'])):
            $json = json_decode($this->attributes['title'], TRUE);
            return isset($json[\App::getLocale()]) ? $json[\App::getLocale()] : '';
        endif;
    }

    public function files() {

        return $this->hasMany('\STALKER_CMS\Core\Uploads\Models\Upload', 'file_type_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['title' => 'required', 'icon' => 'required', 'extensions' => 'required', 'mime_type' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required', 'icon' => 'required', 'extensions' => 'required', 'mime_type' => 'required'];
    }
}