<?php
namespace STALKER_CMS\Core\Uploads\Http\Controllers;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadValidatorController extends ModuleController {

    private $error;
    private $message;
    private $uploaded_file;
    private $file_types;
    private $uploaded_type;

    public function init(UploadedFile $uploaded_file) {

        $this->error = FALSE;
        $this->message = '';
        $this->uploaded_type = NULL;
        $this->uploaded_file = $uploaded_file;
        $this->file_types = parent::getAllowedFileTypes();
        return $this;
    }

    public function passes() {

        $this->valid();
        return !$this->error ? TRUE : FALSE;
    }

    public function valid() {

        if ($this->uploaded_file->isValid()):
            $this->validMimeType();
            $this->validExtensions();
            $this->validSize();
        else:
            $this->error = TRUE;
            $this->message = $this->uploaded_file->getErrorMessage();
        endif;
        return $this;
    }

    public function setUploadedType($mime_type) {

        foreach ($this->file_types as $type):
            if ($mime_type == $this->uploaded_file->getClientMimeType()):
                $this->uploaded_type = $type;
            endif;
        endforeach;
        return $this;
    }

    public function getMessage() {

        return $this->message;
    }

    private function validMimeType() {

        foreach ($this->file_types as $type):
            if ($type['mime'] == strtolower($this->uploaded_file->getClientMimeType())):
                $this->uploaded_type = $type;
                return TRUE;
            endif;
        endforeach;
        $this->error = TRUE;
        $this->message = \Lang::get('root_lang::validation.format_not_supported');
        return FALSE;
    }

    private function validExtensions() {

        if ($this->error || is_null($this->uploaded_type)):
            return FALSE;
        endif;
        foreach (explode('|', $this->uploaded_type['extensions']) as $extension):
            if ($extension == strtolower($this->uploaded_file->getClientOriginalExtension())):
                return TRUE;
            endif;
        endforeach;
        $this->error = TRUE;
        $this->message = \Lang::get('root_lang::validation.format_not_supported');
        return FALSE;
    }

    private function validSize() {

        if ($this->error || is_null($this->uploaded_type)):
            return FALSE;
        endif;
        if ($this->uploaded_file->getClientSize() <= ($this->uploaded_type['max_size'] * 1048576)):
            return TRUE;
        endif;
        $this->error = TRUE;
        $this->message = \Lang::get('root_lang::validation.valid_size') . ': ' . $this->uploaded_type['max_size'] .
            \STALKER_CMS\Vendor\Helpers\array_translate(['ru' => 'Мбайт', 'ru' => 'Mb', 'es' => 'Mb']);
        return FALSE;
    }
}