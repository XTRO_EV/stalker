<?php
namespace STALKER_CMS\Core\Uploads\Http\Controllers;

use STALKER_CMS\Core\Uploads\Models\FileTypes;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class UploadsTypesController extends ModuleController implements CrudInterface {

    protected $model;

    public function __construct(FileTypes $fileTypes) {

        $this->model = $fileTypes;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('core_uploads', 'types');
        return view('core_uploads_views::types.index', ['types' => $this->model->get()]);
    }

    public function create() {
        // TODO: Implement create() method.
    }

    public function store() {
        // TODO: Implement store() method.
    }

    public function edit($id) {
        // TODO: Implement edit() method.
    }

    public function update($id) {

        \PermissionsController::allowPermission('core_uploads', 'types');
        $request = \RequestController::isAJAX()->init();
        $this->model->replace($id, $request);
        return \ResponseController::success(1204)->redirect(route('system.uploads.types.index'))->json();
    }

    public function destroy($id) {

        \PermissionsController::allowPermission('core_uploads', 'types');
        $request = \RequestController::isAJAX()->init();
        $this->model->replace($id, $request);
        return \ResponseController::success(1205)->redirect(route('system.uploads.types.index'))->json();
    }
}