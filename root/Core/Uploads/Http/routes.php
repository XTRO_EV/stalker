<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {
    \Route::resource('uploads', 'UploadsController',
        [
            'only' => ['index', 'store', 'destroy'],
            'names' => [
                'index' => 'system.uploads.index',
                'store' => 'system.uploads.store',
                'destroy' => 'system.uploads.destroy'
            ]
        ]
    );
    \Route::get('uploads/types', ['as' => 'system.uploads.types.index', 'uses' => 'UploadsTypesController@index']);
    \Route::post('uploads/type/{id}/enabled', ['as' => 'system.uploads.types.update', 'uses' => 'UploadsTypesController@update']);
    \Route::delete('uploads/type/{id}/disabled', ['as' => 'system.uploads.types.destroy', 'uses' => 'UploadsTypesController@destroy']);
});
\Route::group(['middleware' => 'public'], function () {
    \Route::match(['get', 'post'], 'files/download/{crypt_id}', ['as' => 'system.uploads.file.download', 'uses' => 'UploadsController@dynamicDownloadFile']);
});