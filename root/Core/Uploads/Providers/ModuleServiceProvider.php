<?php

namespace STALKER_CMS\Core\Uploads\Providers;

use STALKER_CMS\Core\Uploads\Http\Controllers\UploadValidatorController;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider {

    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_uploads_views');
        $this->registerLocalization('core_uploads_lang');
        $this->registerConfig('core_uploads::config', 'Config/uploads.php');
        $this->registerSettings('core_uploads::settings', 'Config/settings.php');
        $this->registerActions('core_uploads::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_uploads::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
    }

    public function register() {

        \App::bind('UploadValidatorController', function () {
            return new UploadValidatorController();
        });
    }
    /********************************************************************************************************************/
    protected function registerBladeDirectives() {

    }
}