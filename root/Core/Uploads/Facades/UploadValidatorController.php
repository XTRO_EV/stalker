<?php
namespace STALKER_CMS\Core\Uploads\Facades;

use Illuminate\Support\Facades\Facade;

class UploadValidatorController extends Facade {

    protected static function getFacadeAccessor() {

        return 'UploadValidatorController';
    }
}