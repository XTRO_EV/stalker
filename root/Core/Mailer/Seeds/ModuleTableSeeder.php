<?php
namespace STALKER_CMS\Core\Mailer\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ModuleTableSeeder extends Seeder {

    public function run() {

        \DB::table('mailer_templates')->insert([
            'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Обратная связь', 'en' => 'Feedback', 'es' => 'Retroalimentación']),
            'path' => 'feedback.blade.php', 'required' => TRUE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
    }

    private function translate(array $trans) {

        return array_first($trans, function ($key, $value) {
            return $key == \App::getLocale();
        });
    }
}