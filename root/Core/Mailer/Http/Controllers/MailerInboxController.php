<?php

namespace STALKER_CMS\Core\Mailer\Http\Controllers;

use Carbon\Carbon;
use STALKER_CMS\Core\Mailer\Models\MailInbox;

class MailerInboxController extends ModuleController {

    protected $model;

    public function __construct(MailInbox $mails) {

        $this->model = $mails;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('core_mailer', 'mailer');
        $request = \RequestController::init();
        $mails = $this->model->with('author');
        if ($request::has('sort_field') && $request::has('sort_direction')):
            foreach (explode(',', $request::get('sort_field')) as $index):
                $mails = $mails->orderBy($index, $request::get('sort_direction'));
            endforeach;
        else:
            $mails = $mails->orderBy('views')->orderBy('created_at', 'DESC');
        endif;
        $this->model->where('views', FALSE)->update(['views' => TRUE, 'updated_at' => Carbon::now()]);
        return view('core_mailer_views::inbox.index', ['mails' => $mails->paginate(10)]);
    }
}