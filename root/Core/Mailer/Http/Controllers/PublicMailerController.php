<?php

namespace STALKER_CMS\Core\Mailer\Http\Controllers;

use Carbon\Carbon;
use \STALKER_CMS\Vendor\Helpers;
use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Mailer\Models\MailInbox;

class PublicMailerController extends ModuleController {

    protected $model;

    public function __construct() {

        $this->model = new MailInbox();
    }

    public function feedback() {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $insert = [
                'content' => json_encode([
                    'name' => $request::input('name'),
                    'email' => $request::input('email'),
                    'phone' => $request::input('phone'),
                    'message' => $request::input('message'),
                ]),
                'author' => \Auth::check() ? \Auth::id() : NULL
            ];
            $request::merge($insert);
            $this->model->insert($request);
            $subject = Helpers\settings(['core_mailer', 'mailer', 'feedback_subject']);
            $emails = explode(',', Helpers\settings(['core_mailer', 'mailer', 'feedback_emails']));
            $this->send($request, $emails, $subject);
            return \ResponseController::success(1700)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function register(\Request $request) {

        $subject = Helpers\array_translate(['ru' => 'Регистрация', 'en' => 'Registration', 'es' => 'Registro']);
        $this->send($request, $request::input('email'), $subject, 'registration');
    }

    public function getNewMessages($getCount = FALSE) {

        return $getCount ?
            $this->model->whereViews(FALSE)->count() :
            $this->model->whereViews(FALSE)->orderBy('created_at', 'DESC')->with('author')->get();
    }

    public function getViewMessages($getCount = FALSE) {

        return $getCount ?
            $this->model->whereViews(TRUE)->count() :
            $this->model->whereViews(TRUE)->orderBy('created_at', 'DESC')->with('author')->get();
    }

    public function getSendMessages($getCount = FALSE) {

        return $getCount ?
            $this->model->count() :
            $this->model->orderBy('created_at', 'DESC')->with('author')->get();
    }

    public function getLastMessages($getCount = 5) {

        return $this->model->orderBy('created_at', 'DESC')->with('author')->take($getCount)->get();
    }

    /***************************************************************************/
    private function send(\Request $request, $emails, $subject = 'NoName', $template = 'feedback') {

        \Mail::send('mails_views::' . $template, ['data' => $request::all()], function ($message) use ($subject, $emails) {
            $message->from(Helpers\settings(['core_mailer', 'mailer', 'from_email']), Helpers\settings(['core_mailer', 'mailer', 'from_name']));
            $message->to($emails)->subject($subject);
        });
    }
}