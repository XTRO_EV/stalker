<?php
namespace STALKER_CMS\Core\Mailer\Http\Controllers;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\View;
use STALKER_CMS\Core\Mailer\Models\MailTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

class TemplatesController extends ModuleController implements CrudInterface {

    protected $model;

    public function __construct(MailTemplate $mailTemplate) {

        $this->model = $mailTemplate;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('core_mailer', 'templates');
        $templates = $this->model->whereLocale(\App::getLocale())->orderBy('updated_at', 'DESC')->get();
        return view('core_mailer_views::templates.index', compact('templates'));
    }

    public function create() {

        \PermissionsController::allowPermission('core_mailer', 'templates');
        $template_content = '';
        if (view()->exists('core_mailer_views::templates.sketch')):
            $template_content = \File::get(view('core_mailer_views::templates.sketch')->getPath());
        endif;
        return view('core_mailer_views::templates.create', compact('template_content'));
    }

    public function store() {

        \PermissionsController::allowPermission('core_mailer', 'templates');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $view_path = base_path('home/Resources/Mails/' . $request::input('path') . '.blade.php');
            $view_path = Helpers\double_slash($view_path);
            if (\File::exists($view_path) === FALSE):
                $request::merge(['path' => $request::input('path') . '.blade.php', 'required' => FALSE, 'locale' => \App::getLocale()]);
                $this->model->insert($request);
                \File::put($view_path, $request::input('content'));
                return \ResponseController::success(1600)->redirect(route('core.mailer.templates.index'))->json();
            else:
                return \ResponseController::error(2610)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {

        \PermissionsController::allowPermission('core_mailer', 'templates');
        $template_content = '';
        $template = $this->model->findOrFail($id);
        $view_path = base_path('/home/Resources/Mails/' . $template->path);
        if (\File::exists(realpath($view_path))):
            $template_content = \File::get(realpath($view_path));
        endif;
        return view('core_mailer_views::templates.edit', compact('template_content', 'template'));
    }

    public function update($id) {

        \PermissionsController::allowPermission('core_mailer', 'templates');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $template = $this->model->findOrFail($id);
            $view_path = base_path('/home/Resources/Mails/' . $template->path);
            $view_path = Helpers\double_slash($view_path);
            \File::put($view_path, $request::input('content'));
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.mailer.templates.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($id) {

        \PermissionsController::allowPermission('core_mailer', 'templates');
        $request = \RequestController::isAJAX()->init();
        try {
            $template = $this->model->findOrFail($id);
            $view_path = base_path('/home/Resources/Mails/' . $template->path);
            if (\File::exists(realpath($view_path))):
                \File::delete(realpath($view_path));
            endif;
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('core.mailer.templates.index'))->json();
        } catch (Exception $e) {
            return \ResponseController::success(2503)->json();
        }
    }
}