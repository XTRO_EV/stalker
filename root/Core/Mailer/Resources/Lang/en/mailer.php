<?php

return [
    'templates' => 'Templates of letters',
    'root_directory' => 'Root directory: /home/Resources/Mails',
    'sort_date_created' => 'Date received',
    'empty' => 'List is empty',
];