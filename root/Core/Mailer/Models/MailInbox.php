<?php
namespace STALKER_CMS\Core\Mailer\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class MailInbox extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'mailer_inbox';
    protected $fillable = ['content', 'views', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $this->content = $request::input('content');
        $this->views = 0;
        $this->user_id = $request::input('author');
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->views = TRUE;
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function getNameAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['name']) ? $content['name'] : NULL;
        endif;
        return NULL;
    }

    public function getEmailAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['email']) ? $content['email'] : NULL;
        endif;
        return NULL;
    }

    public function getPhoneAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['phone']) ? $content['phone'] : NULL;
        endif;
        return NULL;
    }

    public function getMessageAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['message']) ? $content['message'] : NULL;
        endif;
        return NULL;
    }

    public function getAvatarAttribute() {

        if (!empty($this->author) && !empty($this->author->thumbnail)):
            return \STALKER_CMS\Vendor\Helpers\double_slash('uploads/' . $this->author->thumbnail);
        endif;
        return NULL;
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['name' => 'required', 'email' => 'required|email', 'message' => 'required'];
    }

    public static function getUpdateRules() {

        return [];
    }
}