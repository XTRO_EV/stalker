<?php

return [
    'mailer' => [
        'title' => ['ru' => 'Почта', 'en' => 'Mail', 'es' => 'Correo'],
        'options' => [
            ['group_title' => ['ru' => 'Настройка отправки', 'en' => 'Sending setting', 'es' => 'Envío de ajuste']],
            'from_name' => [
                'title' => [
                    'ru' => 'Имя отправителя',
                    'en' => 'Sender name',
                    'es' => 'El nombre del remitente'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => config('mail.from.name')
            ],
            'from_email' => [
                'title' => [
                    'ru' => 'Email отправителя',
                    'en' => 'Sender email',
                    'es' => 'Remitente del correo electrónico'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => config('mail.from.address')
            ],
            ['group_title' => ['ru' => 'Обратная связь', 'en' => 'Feedback', 'es' => 'Realimentación']],
            'feedback_subject' => [
                'title' => [
                    'ru' => 'Тема письма',
                    'en' => 'Letter subject',
                    'es' => 'Sujeto'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => 'Feedback'
            ],
            'feedback_emails' => [
                'title' => [
                    'ru' => 'Адреса получателей',
                    'en' => 'Recipient emails',
                    'es' => 'Mensajes de correo electrónico del destinatario'
                ],
                'note' => [
                    'ru' => 'Перечислить через запятую',
                    'en' => 'List separated by commas',
                    'es' => 'Lista separada por comas'
                ],
                'type' => 'text',
                'value' => config('mail.from.address')
            ]
        ]
    ]
];