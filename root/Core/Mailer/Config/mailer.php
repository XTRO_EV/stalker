<?php

return [
    'package_name' => 'core_mailer',
    'package_title' => ['ru' => 'Почтовый модуль', 'en' => 'Mailing module', 'es' => 'Módulo de correo'],
    'package_icon' => 'zmdi zmdi-mail-send',
    'version' => [
        'ver' => 0.2,
        'date' => '01.01.2016'
    ]
];