<?php
namespace STALKER_CMS\Core\Mailer\Facades;

use Illuminate\Support\Facades\Facade;

class PublicMailer extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicMailerController';
    }
}