<?php

namespace STALKER_CMS\Core\Seo\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;

trait SeoTrait {

    public function getPageTitleAttribute() {

        if (!empty($this->attributes['seo_title'])):
            return $this->attributes['seo_title'];
        else:
            return $this->attributes['title'];
        endif;
    }

    public function getPageDescriptionAttribute() {

        if (!empty($this->attributes['seo_description'])):
            return $this->attributes['seo_description'];
        else:
            return '';
        endif;
    }

    public function getPageH1Attribute() {

        if (!empty($this->attributes['seo_h1'])):
            return $this->attributes['seo_h1'];
        else:
            return $this->attributes['title'];
        endif;
    }

    public function getPageUrlAttribute() {

        if (!empty($this->attributes['seo_url'])):
            return $this->attributes['seo_url'];
        else:
            return $this->attributes['id'];
        endif;
    }

    public function uniqueSeoURL($url, $id = NULL) {

        if (!empty($url)):
            if (in_array($url, config('core_content::config.forbidden_urls'))):
                throw new \Exception(\Lang::get('root_lang::codes.2507'), 2507);
            elseif (is_null($id) && static::whereExist(['seo_url' => $url])):
                throw new \Exception(\Lang::get('root_lang::codes.2507'), 2507);
            elseif (is_numeric($id) && static::where('id', '!=', $id)->where('seo_url', $url)->exists()):
                throw new \Exception(\Lang::get('root_lang::codes.2507'), 2507);
            endif;
        endif;
    }
}