<?php

namespace STALKER_CMS\Core\Seo\Providers;

use STALKER_CMS\Vendor\Providers\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider {

    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_seo_views');
        $this->registerLocalization('core_seo_lang');
        $this->registerConfig('core_seo::config', 'Config/seo.php');
        $this->registerSettings('core_seo::settings', 'Config/settings.php');
        $this->registerActions('core_seo::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_seo::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
    }

    public function register() {

    }

    /********************************************************************************************************************/
    public function registerBladeDirectives() {

    }
}