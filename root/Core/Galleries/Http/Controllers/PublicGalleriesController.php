<?php

namespace STALKER_CMS\Core\Galleries\Http\Controllers;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Helpers;
use STALKER_CMS\Core\Galleries\Models\Gallery;
use Illuminate\Database\Eloquent\Collection;

class PublicGalleriesController extends ModuleController {

    protected $model;

    public function __construct() {

        $this->model = new Gallery();
    }

    public function images($symbolicCode) {

        if ($gallery = $this->model->whereSlug($symbolicCode)->first()):
            return $gallery->photos;
        else:
            return new Collection();
        endif;
    }
}