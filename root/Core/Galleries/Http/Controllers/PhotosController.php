<?php
namespace STALKER_CMS\Core\Galleries\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use League\Flysystem\Exception;
use STALKER_CMS\Core\Galleries\Models\Gallery;
use STALKER_CMS\Core\Galleries\Models\Photo;

use \STALKER_CMS\Vendor\Helpers as Helpers;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Class PhotosController
 * @package STALKER_CMS\Core\Galleries\Http\Controllers
 */
class PhotosController extends ModuleController implements CrudInterface {

    use ModelTrait;
    /**
     * Модель
     * @var Photo
     */
    protected $model;
    protected $gallery;

    public function __construct(Photo $photo, Gallery $gallery) {

        $this->model = $photo;
        $this->gallery = $gallery;
        $this->middleware('auth');
    }

    /**
     * Просмотр галереи
     * @param null $id - ID галереи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id = NULL) {

        \PermissionsController::allowPermission('core_galleries', 'images');
        $gallery = $this->gallery->whereFind(['id' => $id]);
        $photos = $gallery->photos()->get();
        return view('core_galleries_views::photos.index',
            [
                'gallery' => $gallery,
                'photos' => $photos,
                'allowed_types' => self::getAllowedFileTypes()
            ]
        );
    }

    /**
     * Загрузка файла на сервер
     * @param null $id - ID галереи
     * @return mixed
     */
    public function store($id = NULL) {

        \PermissionsController::allowPermission('core_galleries', 'images');
        $request = \RequestController::isAJAX()->init();
        $valid = \GalleryValidatorController::init($request::file('file'));
        if ($valid->passes()):
            $this->gallery->whereFind(['id' => $id]);
            if ($image = $this->uploadImage($request)):
                $image['gallery_id'] = $id;
                $image['photo'] = NULL;
                $image['thumbnail'] = NULL;
                $image['order'] = $this->model->nextOrder($id);
                $request::merge($image);
                $this->model->insert($request);
            endif;
            return \ResponseController::success(200)->redirect(route('system.galleries.photos_index', $id))->json();
        else:
            return \ResponseController::error(0)->set('errorText', $valid->getMessage())->json();
        endif;
    }

    public function create() {
        // TODO: Implement create() method.
    }

    public function edit($id) {
        // TODO: Implement edit() method.
    }

    public function update($id) {
        // TODO: Implement update() method.
    }

    public function destroy($id) {

        \PermissionsController::allowPermission('core_galleries', 'images');
        $request = \RequestController::isAJAX()->init();
        if (is_numeric($id)):
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('system.galleries.photos_index', $request::input('gallery')))->json();
        elseif (is_string($id) && $id == 'selected'):
            $this->deleteFiles($this->model->whereIn('id', $request::input('files'))->get());
            return \ResponseController::success(1203)->redirect(route('system.galleries.photos_index', $request::input('gallery')))->json();
        elseif (is_string($id) && $id == 'all'):
            $this->deleteFiles($this->model->all());
            return \ResponseController::success(1203)->redirect(route('system.galleries.photos_index', $request::input('gallery')))->json();
        endif;
        return \ResponseController::error(2503)->json();
    }

    /**
     * @param $request
     * @return null
     * @throws \Exception
     */
    public function uploadImage($request) {

        if ($photo = Helpers\trim_base64_image($request::input('photo'))):
            $thumbnail = Helpers\trim_base64_image($request::input('thumbnail'));
            $upload_directory = Helpers\setDirectory(Helpers\settings(['core_galleries', 'galleries', 'photo_dir']));
            $upload_directory_thumbnail = Helpers\setDirectory(Helpers\settings(['core_galleries', 'galleries', 'thumbnail_dir']));
            $fileName = time() . "_" . rand(1000, 1999) . '.jpg';
            $photoPath = Helpers\double_slash($upload_directory . '/' . $fileName);
            $thumbnailPath = Helpers\double_slash($upload_directory_thumbnail . '/' . $fileName);
            \Storage::put($photoPath, base64_decode($photo));
            \Storage::put($thumbnailPath, base64_decode($thumbnail));
            $uploaded['path'] = Helpers\add_first_slash($photoPath);
            $uploaded['thumbnail_path'] = Helpers\add_first_slash($thumbnailPath);
            $uploaded['file_size'] = \Storage::size($photoPath);
            $uploaded['mime_type'] = \Storage::mimeType($photoPath);
            return $uploaded;
        else:
            throw new \Exception(\Lang::get('root_lang::codes.2506'), 500);
        endif;
    }

    /************************************************************************************/
    private function deleteFiles(Collection $files) {

        foreach ($files as $file):
            $this->model->remove($file->id);
        endforeach;
    }
}