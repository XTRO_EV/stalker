<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {
    \Route::resource('galleries', 'GalleriesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'system.galleries.index',
                'create' => 'system.galleries.create',
                'store' => 'system.galleries.store',
                'edit' => 'system.galleries.edit',
                'update' => 'system.galleries.update',
                'destroy' => 'system.galleries.destroy'
            ]
        ]
    );
    \Route::resource('galleries.photos', 'PhotosController',
        [
            'only' => ['index', 'store', 'destroy'],
            'names' => [
                'index' => 'system.galleries.photos_index',
                'store' => 'system.galleries.photos_store',
                'destroy' => 'system.galleries.photos_destroy'
            ]
        ]
    );
    \Route::delete('galleries/photos/{photo_id}/destroy', ['as' => 'system.galleries.photos_destroy', 'uses' => 'PhotosController@destroy']);
});