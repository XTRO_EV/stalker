<?php

return [
    'package' => 'core_galleries',
    'title' => ['ru' => 'Галереи', 'en' => 'Galleries', 'es' => 'Galerías'],
    'route' => 'system.galleries.index',
    'icon' => 'zmdi zmdi-collection-image',
    'menu_child' => []
];