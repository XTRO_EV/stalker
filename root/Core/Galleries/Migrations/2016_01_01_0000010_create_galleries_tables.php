<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleriesTables extends Migration {

    public function up() {

        Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->default(0)->unsigned()->nullable()->index();
            $table->integer('unit_id')->default(0)->unsigned()->nullable()->index();
            $table->string('slug', 50)->nullable()->unique();
            $table->string('title', 50)->nullable();
            $table->boolean('resizable')->default(1)->unsigned()->nullable();
            $table->string('aspect_ratio', 10)->nullable();
            $table->integer('crop_wight')->unsigned()->nullable();
            $table->integer('crop_height')->unsigned()->nullable();
            $table->integer('crop_wight_thumbnail')->default(200)->unsigned()->nullable();
            $table->integer('crop_height_thumbnail')->default(200)->unsigned()->nullable();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('galleries');
    }
}

