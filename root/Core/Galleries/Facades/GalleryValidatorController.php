<?php
namespace STALKER_CMS\Core\Galleries\Facades;

use Illuminate\Support\Facades\Facade;

class GalleryValidatorController extends Facade {

    protected static function getFacadeAccessor() {

        return 'GalleryValidatorController';
    }
}