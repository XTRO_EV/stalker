<?php
namespace STALKER_CMS\Core\Galleries\Facades;

use Illuminate\Support\Facades\Facade;

class PublicGallery extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicGalleriesController';
    }
}