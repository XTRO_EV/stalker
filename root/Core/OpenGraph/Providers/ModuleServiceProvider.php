<?php

namespace STALKER_CMS\Core\OpenGraph\Providers;

use STALKER_CMS\Core\OpenGraph\Http\Controllers\PublicOpenGraphController;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider {

    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_open_graph_views');
        $this->registerLocalization('core_open_graph_lang');
        $this->registerConfig('core_open_graph::config', 'Config/open_graph.php');
        $this->registerSettings('core_open_graph::settings', 'Config/settings.php');
        $this->registerActions('core_open_graph::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_open_graph::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
    }

    public function register() {

        \App::bind('PublicOpenGraphController', function () {
            return new PublicOpenGraphController();
        });
    }

    /********************************************************************************************************************/
    public function registerBladeDirectives() {

    }
}