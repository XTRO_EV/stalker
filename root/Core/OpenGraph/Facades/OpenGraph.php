<?php
namespace STALKER_CMS\Core\OpenGraph\Facades;

use Illuminate\Support\Facades\Facade;

class OpenGraph extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicOpenGraphController';
    }
}