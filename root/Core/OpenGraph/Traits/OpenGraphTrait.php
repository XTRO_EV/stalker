<?php

namespace STALKER_CMS\Core\OpenGraph\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;

trait OpenGraphTrait {

    public function getPageOpenGraphAttribute() {

        if (!empty($this->attributes['open_graph'])):
            return $this->attributes['open_graph'];
        else:
            return '';
        endif;
    }

    public function getOgTitleAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                return json_decode($this->attributes['open_graph'], TRUE)['og:title'];
            else:
                return NULL;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
    }

    public function getOgDescriptionAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                return json_decode($this->attributes['open_graph'], TRUE)['og:description'];
            else:
                return NULL;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
    }

    public function getOgImagesAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                return json_decode($this->attributes['open_graph'], TRUE)['og:image'];
            else:
                return NULL;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
    }

    public function getOgImageFirstAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                if ($images = json_decode($this->attributes['open_graph'], TRUE)['og:image']):
                    return isset($images[0]) ? asset('uploads/pages/' . $images[0]) : NULL;
                endif;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
        return NULL;
    }

    public function getOgImageSecondAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                if ($images = json_decode($this->attributes['open_graph'], TRUE)['og:image']):
                    return isset($images[1]) ? asset('uploads/pages/' . $images[1]) : NULL;
                endif;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
        return NULL;
    }

    public function getOgImageThirdAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                if ($images = json_decode($this->attributes['open_graph'], TRUE)['og:image']):
                    return isset($images[2]) ? asset('uploads/pages/' . $images[2]) : NULL;
                endif;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
        return NULL;
    }

}