<?php

return [
    'package_name' => 'core_open_graph',
    'package_title' => ['ru' => 'Модуль Open Graph', 'en' => 'Open Graph module', 'es' => 'Módulo de Open Graph'],
    'package_icon' => 'zmdi zmdi-share',
    'version' => [
        'ver' => 0.1,
        'date' => '01.01.2016'
    ]
];
