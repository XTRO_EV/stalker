<?php

return [
    'package_name' => 'core_install',
    'package_title' => [
        'ru' => 'Модуль установки системы',
        'en' => 'The module installation',
        'es' => 'La instalación del módulo'
    ],
    'package_icon' => 'zmdi-puzzle-piece',
    'languages' => ['ru' => 'Русский', 'en' => 'English', 'es' => 'Español'],
    'version' => [
        'ver' => 0.1,
        'date' => '01.01.2016'
    ],
];
