<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration {

    public function up() {

        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 10)->nullable()->index();
            $table->string('title', 30)->nullable();
            $table->string('iso_639_1', 2)->nullable();
            $table->string('iso_639_2', 3)->nullable();
            $table->string('iso_639_3', 3)->nullable();
            $table->string('code', 3)->nullable();
            $table->boolean('active')->default(0)->unsigned()->nullable();
            $table->boolean('default')->default(0)->unsigned()->nullable();
            $table->boolean('required')->default(0)->unsigned()->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('languages');
    }
}
