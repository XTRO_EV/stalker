<?php

namespace STALKER_CMS\Core\Install\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

    protected $namespace = 'STALKER_CMS\Core\Install\Http\Controllers';

    public function boot(Router $router) {

        parent::boot($router);
    }

    public function map(Router $router) {

        $router->group(['namespace' => $this->namespace], function ($router) {
            require_once(__DIR__ . '/../Http/routes.php');
        });
    }
}
