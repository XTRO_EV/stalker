<?php

return [
    'page_title' => 'Configuration Complete',
    'page_description' => '',
    'title' => 'Done',
    'description' => 'Setup completed successfully'
];