<?php
namespace STALKER_CMS\Core\Auth\Facades;

use Illuminate\Support\Facades\Facade;

class InstallController extends Facade {

    protected static function getFacadeAccessor() {

        return 'InstallController';
    }
}