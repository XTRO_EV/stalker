<?php

namespace STALKER_CMS\Core\Auth\Http\Controllers;

class DashboardController extends ModuleController {

    public function __construct() {

        $this->middleware('auth');
    }

    public function index() {

        $template = \Auth::user()->group->dashboard;
        if (view()->exists("core_system_views::dashboards.$template")):
            return view("core_system_views::dashboards.$template");
        else:
            return view('core_system_views::dashboards.admin');
        endif;
    }
}
