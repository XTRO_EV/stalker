<?php

namespace STALKER_CMS\Vendor\Http\Controllers;

use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends ModuleController {

    use ResetsPasswords;

    public function __construct() {

        $this->middleware('guest');
    }
}
