<?php
namespace STALKER_CMS\Core\Auth\Facades;

use Illuminate\Support\Facades\Facade;

class Dashboard extends Facade {

    protected static function getFacadeAccessor() {

        return 'DashboardController';
    }
}