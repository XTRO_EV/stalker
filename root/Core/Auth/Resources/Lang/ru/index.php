<?php

return [
    'page_title' => 'Авторизация',
    'page_description' => '',
    'form' => [
        'title' => 'Вход на сайт',
        'login_field' => 'Логин',
        'password' => 'Пароль'
    ]
];