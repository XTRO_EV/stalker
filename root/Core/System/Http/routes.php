<?php
###########################################################################################
#Роуты для модуля Настройки
###########################################################################################
\Route::group(['prefix' => 'admin/system/settings', 'middleware' => 'secure'], function () {
    \Route::get('/', ['as' => 'system.settings.index', 'uses' => 'SettingsController@index']);
    \Route::post('update', ['as' => 'system.settings.update', 'uses' => 'SettingsController@update']);
    \Route::get('/rebuild', ['as' => 'system.settings.rebuild', 'uses' => 'SettingsController@rebuild']);
    \Route::get('/cache', ['as' => 'system.settings.cache.create', 'uses' => 'SettingsController@createConfigCash']);
    \Route::get('/clear', ['as' => 'system.settings.cache.clear', 'uses' => 'SettingsController@clearConfigCash']);
    \Route::get('/phpInformation', ['as' => 'system.settings.php', 'uses' => 'SettingsController@phpInformation']);
});
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {
    \Route::get('settings', ['as' => 'cms.settings.index', 'uses' => 'SettingsController@settings']);
    \Route::post('settings', ['as' => 'cms.settings.update', 'uses' => 'SettingsController@settingsStore']);
});
###########################################################################################
#Роуты для модуля Модули
###########################################################################################
\Route::group(['prefix' => 'admin/system/modules', 'middleware' => 'secure'], function () {
    \Route::get('/', ['as' => 'system.modules.index', 'uses' => 'ModulesController@index']);
    \Route::get('/rebuild', ['as' => 'system.modules.rebuild', 'uses' => 'ModulesController@rebuild']);
    \Route::post('/enable', ['as' => 'system.modules.update', 'uses' => 'ModulesController@update']);
    \Route::delete('/disable', ['as' => 'system.modules.disable', 'uses' => 'ModulesController@destroy']);
    \Route::get('/boot', ['as' => 'system.modules.boot', 'uses' => 'ModulesController@boot']);
    \Route::post('/boot', ['as' => 'system.modules.boot.update', 'uses' => 'ModulesController@bootUpdate']);
});
###########################################################################################
#Роуты для подмодуля Решения
###########################################################################################
\Route::group(['prefix' => 'admin' . '/system/modules/solutions', 'middleware' => 'secure'], function () {
    \Route::get('/', ['as' => 'system.modules.solutions.index', 'uses' => 'ModulesSolutionsController@index']);
    \Route::post('enable', ['as' => 'system.modules.solutions.enable', 'uses' => 'ModulesSolutionsController@enable']);
    \Route::delete('disable', ['as' => 'system.modules.solutions.disable', 'uses' => 'ModulesSolutionsController@disable']);
});
###########################################################################################
#Роуты для модуля Группы
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function () {
    \Route::get('groups/{group_id}/accesses', ['as' => 'system.groups.accesses-index', 'uses' => 'PermissionsController@index'])->where(['id' => '[0-9]+']);
    \Route::put('groups/{group_id}/accesses', ['as' => 'system.groups.accesses-update', 'uses' => 'PermissionsController@update'])->where(['id' => '[0-9]+']);
    \Route::resource('groups', 'GroupsController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'system.groups.index',
                'create' => 'system.groups.create',
                'store' => 'system.groups.store',
                'edit' => 'system.groups.edit',
                'update' => 'system.groups.update',
                'destroy' => 'system.groups.destroy',
            ]
        ]
    );
});
###########################################################################################
#Роуты для модуля Пользователи
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function () {
    \Route::resource('users', 'UsersController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'system.users.index',
                'create' => 'system.users.create',
                'store' => 'system.users.store',
                'edit' => 'system.users.edit',
                'update' => 'system.users.update',
                'destroy' => 'system.users.destroy',
            ]
        ]
    );
});