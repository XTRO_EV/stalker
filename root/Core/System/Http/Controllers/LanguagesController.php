<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Models\Languages;

class LanguagesController extends ModuleController implements CrudInterface {

    public function index() {
        // TODO: Implement index() method.
    }

    public function create() {
        // TODO: Implement create() method.
    }

    public function store() {
        // TODO: Implement store() method.
    }

    public function edit($id) {
        // TODO: Implement edit() method.
    }

    public function update($id) {
        // TODO: Implement update() method.
    }

    public function destroy($id) {
        // TODO: Implement destroy() method.
    }

    public function getLanguages() {

        return Languages::whereActive(TRUE)->lists('title', 'slug');
    }
}