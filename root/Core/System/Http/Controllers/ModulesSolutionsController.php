<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Vendor\Helpers;
use STALKER_CMS\Vendor\Models\Packages;

/**
 * Class ModulesSolutionsController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class ModulesSolutionsController extends ModuleController {

    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Список доступных решений на базе установленных модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_system', 'modules_solutions');
        $solutions_packages = $this->getAvailableSolutions();
        return view('core_system_views::solutions.index', compact('solutions_packages'));
    }

    public function enable() {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, ['solution' => 'required'])):
            $solutions_packages = $this->getAvailableSolutions();
            if (isset($solutions_packages[$request::input('solution')]) && $solutions_packages[$request::input('solution')]['enabled'] == FALSE):
                $solution = $solutions_packages[$request::input('solution')];
                if ($package = Packages::whereSlug($solution['package_name'])->first()):
                    if (!empty($package->install_path)):
                        \PackagesController::enabledPackage($package);
                    endif;
                else:
                    $package = new Packages();
                    $package->slug = $solution['package_name'];
                    $package->title = json_encode($solution['package_title']);
                    $package->description = json_encode($solution['package_description']);
                    $package->relations = implode('|', $solution['relations']);
                    $package->order = Packages::where('order', '<', 1000)->max('order') + 1;
                    $package->save();
                    \PackagesController::installPackage($package);
                    return \ResponseController::success(200)->redirect(route('system.modules.rebuild', ['redirect_route' => 'system.modules.solutions.index']))->json();
                endif;
                return \ResponseController::success(1204)->redirect(route('system.modules.solutions.index'))->json();
            endif;
            return \ResponseController::success(406)->redirect(route('system.modules.solutions.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function disable() {

    }

    private function getAvailableSolutions() {

        $available_solutions_packages = new Collection();
        $directory_solutions = base_path('root/Solutions');
        foreach (\File::directories($directory_solutions) as $solution_directory):
            $package_config = realpath($solution_directory . '/Config/' . strtolower(basename($solution_directory) . '.php'));
            if (\File::exists($package_config)):
                $show_package = FALSE;
                $solution_package = include($package_config);
                if (!empty($solution_package['relations']) && is_array($solution_package['relations'])):
                    foreach ($solution_package['relations'] as $relation_package_name):
                        if (\PermissionsController::isPackageEnabled($relation_package_name)):
                            $show_package = TRUE;
                            break;
                        endif;
                    endforeach;
                else:
                    $show_package = TRUE;
                endif;
                if ($show_package):
                    $solution_package['enabled'] = FALSE;
                    $solution_package['composer_config'] = NULL;
                    $solution_package['install_path'] = realpath($solution_directory);
                    if (\PermissionsController::isPackageInstalled($solution_package['package_name']) === FALSE):
                        $available_solutions_packages[$solution_package['package_name']] = $solution_package;
                    endif;
                endif;
            endif;
        endforeach;
        return $available_solutions_packages;
    }
}