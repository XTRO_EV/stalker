<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Vendor\Helpers;
use STALKER_CMS\Vendor\Models\Packages;

/**
 * Class ModulesController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class ModulesController extends ModuleController {

    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Список доступных модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $packages = new Collection();
        foreach (Packages::orderBy('required', 'DESC')->orderBy('order')->get() as $package):
            $packages[$package->slug] = $package;
        endforeach;
        return view('core_system_views::modules.index', compact('packages'));
    }

    public function rebuild(\Request $request) {

        $redirect_route = 'system.modules.index';
        if($request::has('redirect_route')):
            $redirect_route = $request::input('redirect_route');
        endif;
        \PermissionsController::allowPermission('core_system', 'modules');
        foreach (Packages::whereEnabled(TRUE)->get() as $package):
            \PackagesController::publishesPackageAssets($package);
        endforeach;
        \Artisan::call('config:cache', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        return redirect()->route($redirect_route);
    }

    /**
     * Последовательность загрузки модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function boot() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $packages = Packages::getNestedPackages();
        return view('core_system::modules.boot', compact('packages'));
    }

    /**
     * Отключение модулей
     * Установка и включение модулей
     * @return \Illuminate\Http\JsonResponse
     */
    public function update() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $request = \RequestController::isAJAX()->init();
        foreach (Packages::whereEnabled(TRUE)->whereRequired(FALSE)->get() as $package):
            if ($request::has($package->slug) === FALSE):
                \PackagesController::disabledPackage($package);
            endif;
        endforeach;
        foreach (Packages::whereEnabled(FALSE)->whereRequired(FALSE)->get() as $package):
            if ($request::has($package->slug)):
                if (empty($package->install_path)):
                    \PackagesController::installPackage($package);
                else:
                    \PackagesController::enabledPackage($package);
                endif;
            endif;
        endforeach;
        return \ResponseController::success(200)->redirect(route('system.modules.rebuild'))->json();
    }

    /**
     * Выключение модуля
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $request = \RequestController::isAJAX()->init();
        if ($package = Packages::whereId($request::get('package'))->whereEnabled(TRUE)->whereRequired(FALSE)->first()):
            \PackagesController::disabledPackage($package);
            return \ResponseController::success(200)->redirect(route('system.modules.index'))->json();
        endif;
        return \ResponseController::error(403)->json();
    }

    /**
     * Сохранение последовательности загрузки модулей
     * @return \Illuminate\Http\JsonResponse
     */
    public function bootUpdate() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $request = \RequestController::isAJAX()->init();
        Helpers\tad($request::all());
        return \ResponseController::success(200)->json();
    }
}