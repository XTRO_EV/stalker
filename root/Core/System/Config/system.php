<?php

return [
    'package_name' => 'core_system',
    'package_title' => [
        'ru' => 'Системный модуль',
        'en' => 'System module',
        'es' => 'Módulo de sistema'
    ],
    'package_icon' => 'zmdi zmdi-settings-square',
    'version' => [
        'ver' => 0.9,
        'date' => '01.01.2016'
    ]
];
