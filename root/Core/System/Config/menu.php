<?php

return [
    'package' => 'core_system',
    'title' => ['ru' => 'Система', 'en' => 'System', 'es' => 'Sistema'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-settings-square',
    'menu_child' => [
        'settings' => [
            'title' => ['ru' => 'Настройки', 'en' => 'Settings', 'es' => 'Ajustes'],
            'route' => 'system.settings.index',
            'icon' => 'zmdi zmdi-settings'
        ],
        'modules' => [
            'title' => ['ru' => 'Модули', 'en' => 'Modules', 'es' => 'Módulos'],
            'route' => 'system.modules.index',
            'icon' => 'zmdi zmdi-view-module'
        ],
        'modules_solutions' => [
            'title' => ['ru' => 'Доступные решения', 'en' => 'Available solutions', 'es' => 'Las soluciones disponibles'],
            'route' => 'system.modules.solutions.index',
            'icon' => 'zmdi zmdi-widgets'
        ],
        'groups' => [
            'title' => ['ru' => 'Группы', 'en' => 'Groups', 'es' => 'Grupos'],
            'route' => 'system.groups.index',
            'icon' => 'zmdi zmdi-accounts-list'
        ],
        'users' => [
            'title' => ['ru' => 'Пользователи', 'en' => 'Users', 'es' => 'Usuarios'],
            'route' => 'system.users.index',
            'icon' => 'zmdi zmdi-accounts-alt'
        ]
    ]
];