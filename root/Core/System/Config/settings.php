<?php
return [
    'settings' => [
        'title' => ['ru' => 'Системные', 'en' => 'System', 'es' => 'Sistema'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Main', 'es' => 'Los principales']],
            'base_locale' => [
                'title' => ['ru' => 'Основной язык системы', 'en' => 'The main system language', 'es' => 'El idioma principal del sistema'],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => env('APP_LOCALE', 'ru')
            ],
            'services_mode' => [
                'title' => ['ru' => 'Режим обслуживания', 'en' => 'Maintenance mode', 'es' => 'Modo de mantenimiento'],
                'note' => [
                    'ru' => 'Позволяет «отключать» приложение, в момент обновления',
                    'en' => 'It allows you to "turn off" the application, at the time of renovation',
                    'es' => 'Le permite "apagar" la aplicación, en el momento de la renovación'
                ],
                'type' => 'checkbox',
                'value' => ''
            ],
            'debug_mode' => [
                'title' => ['ru' => 'Режим отладки', 'en' => 'Debug mode', 'es' => 'Modo de depuración'],
                'note' => [
                    'ru' => 'Позволяет включать режим отладки для анализа ошибок',
                    'en' => 'It allows you to enable debug mode for error analysis',
                    'es' => 'Se le permite activar el modo de depuración para el análisis de errores'
                ],
                'type' => 'checkbox',
                'value' => env('APP_DEBUG', false)
            ]
        ]
    ],
    'users' => [
        'title' => ['ru' => 'Пользователи', 'en' => 'Users', 'es' => 'Usuarios'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Los principales', 'es' => 'Principal']],
            'avatar_dir' => [
                'title' => [
                    'ru' => 'Каталог хранения аватаров',
                    'en' => 'Catalog storage avatars',
                    'es' => 'Avatares almacenamiento catálogo'
                ],
                'note' => [
                    'ru' => 'Корневой каталог: /public/uploads',
                    'en' => 'Root directory: /public/uploads',
                    'es' => 'Directorio raíz: /public/uploads'
                ],
                'type' => 'text',
                'value' => 'avatars'
            ]
        ]
    ]
];