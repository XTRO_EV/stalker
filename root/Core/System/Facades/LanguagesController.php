<?php
namespace STALKER_CMS\Core\System\Facades;

use Illuminate\Support\Facades\Facade;

class LanguagesController extends Facade {

    protected static function getFacadeAccessor() {

        return 'LanguagesController';
    }
}