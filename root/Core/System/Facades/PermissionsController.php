<?php
namespace STALKER_CMS\Core\System\Facades;

use Illuminate\Support\Facades\Facade;

class PermissionsController extends Facade {

    protected static function getFacadeAccessor() {

        return 'PermissionsController';
    }
}