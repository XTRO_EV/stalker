<?php
namespace STALKER_CMS\Core\System\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use STALKER_CMS\Core\System\Facades\LanguagesController;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Group extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'groups';
    protected $fillable = ['slug', 'title', 'dashboard', 'start_url', 'required'];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function insert($request) {

        $title = [];
        foreach (LanguagesController::getLanguages() as $lang_slug => $lang_title):
            $title[$lang_slug] = $request::input('title');
        endforeach;
        $this->slug = strtolower($request::input('slug'));
        $this->title = json_encode($title);
        $this->dashboard = strtolower($request::input('slug'));
        $this->start_url = strtolower($request::input('slug'));
        $this->required = FALSE;
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $title = json_decode($model->title_original, TRUE);
        $title[\App::getLocale()] = $request::input('title');
        $model->slug = strtolower($request::input('slug'));
        $model->attributes['title'] = json_encode($title);
        $model->dashboard = strtolower($request::input('dashboard'));
        $model->start_url = strtolower($request::input('start_url'));
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        User::whereGroupId($id)->update(['group_id' => NULL]);
        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getTitleAttribute() {

        if (!empty($this->attributes['title'])):
            $json = json_decode($this->attributes['title'], TRUE);
            return isset($json[\App::getLocale()]) ? $json[\App::getLocale()] : '';
        endif;
    }

    public function getTitleOriginalAttribute() {

        return $this->attributes['title'];
    }

    public function users() {

        return $this->hasMany('\STALKER_CMS\Core\System\Models\User', 'id', 'group_id');
    }

    public function users_count() {

        return User::where('group_id', $this->id)->count();
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slug' => 'required|alpha', 'title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['slug' => 'required|alpha', 'title' => 'required', 'dashboard' => 'required|alpha', 'start_url' => 'required|alpha'];
    }
}