<?php

namespace STALKER_CMS\Core\System\Models;

use STALKER_CMS\Vendor\Models\BaseModel;

class Sessions extends BaseModel {

    protected $table = 'sessions';
    protected $fillable = [];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = [];

}
