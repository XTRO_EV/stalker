<?php

namespace STALKER_CMS\Core\System\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class User extends Authenticatable implements ModelInterface {

    use ModelTrait;

    protected $table = 'users';
    protected $fillable = ['group_id', 'locale', 'name', 'login', 'password', 'active'];
    protected $hidden = ['password', 'remember_token'];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'disabled_at', 'last_login'];

    public function insert($request) {

        $this->group_id = $request::input('group_id');
        $this->locale = \App::getLocale();
        $this->name = $request::input('name');
        $this->email = $request::input('email');
        $this->login = $request::input('email');
        $this->password = bcrypt($request::input('password'));
        $this->active = $request::has('active') ? TRUE : FALSE;
        $this->approve = $request::has('active') ? TRUE : FALSE;
        $this->country = $request::input('country');
        $this->city = $request::input('city');
        $this->address = $request::input('address');
        $this->birth_date = $request::input('birth_date') ? Carbon::parse($request::input('birth_date'))->format('Y-m-d 00:00:00') : NULL;
        $this->age = $request::input('age');
        $this->sex = $request::input('sex');
        $this->phone = $request::input('phone');
        $this->skype = $request::input('skype');
        $this->social = json_encode($request::input('social'));
        $this->photo = $request::has('photo') ? $request::input('photo') : NULL;
        $this->thumbnail = $request::has('thumbnail') ? $request::input('thumbnail') : NULL;
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->group_id = $request::input('group_id');
        $model->name = $request::input('name');
        $model->email = $request::input('email');
        $model->login = $request::input('email');
        if ($request::input('password') != ''):
            $model->password = bcrypt($request::input('password'));
        endif;
        $model->active = $request::has('active') ? TRUE : FALSE;
        $model->country = $request::input('country');
        $model->city = $request::input('city');
        $model->address = $request::input('address');
        $model->birth_date = $request::input('birth_date') ? Carbon::parse($request::input('birth_date'))->format('Y-m-d 00:00:00') : NULL;
        $model->age = $request::input('age');
        $model->sex = $request::input('sex');
        $model->phone = $request::input('phone');
        $model->skype = $request::input('skype');
        $model->social = json_encode($request::input('social'));
        $model->photo = $request::input('photo');
        $model->thumbnail = $request::input('thumbnail');
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {
        // TODO: Implement remove() method.
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function group() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\Group', 'id', 'group_id');
    }

    public function language() {

        return $this->hasOne('\STALKER_CMS\Vendor\Models\Languages', 'id', 'language_id');
    }

    public function session() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\Sessions', 'user_id', 'id');
    }

    public function getLastLoginAttribute() {

        if ($this->attributes['last_login']):
            \Carbon\Carbon::setLocale(\App::getLocale());
            return \Carbon\Carbon::parse($this->attributes['last_login'])->diffForHumans();
        else:
            return '<span class="c-red">' . \Lang::get('core_system_lang::users.not_logged') . '</span >';
        endif;
    }

    public function getActiveStatusAttribute() {

        if ($this->attributes['active']):
            return '<span class="c-green">' . \Lang::get('core_system_lang::users.active') . '</span>';
        else:
            return '<span class="c-red">' . \Lang::get('core_system_lang::users.blocked_by') . ' ' . \Carbon\Carbon::parse($this->attributes['disabled_at'])->format('d.m.Y') . '</span>';
        endif;
    }

    public function getIsOnlineAttribute() {

        if (is_object($this->session)):
            return (time() - 600) > $this->session->last_activity
                ? '<span class="c-red">' . \Lang::get('core_system_lang::users.offline') . '</span>'
                : '<span class="c-green">' . \Lang::get('core_system_lang::users.online') . '</span>';
        else:
            return '<span class="c-red">' . \Lang::get('core_system_lang::users.offline') . '</span>';
        endif;

    }

    public function getAvatarAttribute() {

        if (!empty($this->attributes['photo'])):
            return \STALKER_CMS\Vendor\Helpers\double_slash('uploads/' . $this->attributes['photo']);
        endif;
    }

    public function getAvatarThumbnailAttribute() {

        if (!empty($this->attributes['thumbnail'])):
            return \STALKER_CMS\Vendor\Helpers\double_slash('uploads/' . $this->attributes['thumbnail']);
        endif;
    }

    /***************************************************************************************************************/
    public static function getAuthRules() {

        return ['login' => 'required|email', 'password' => 'required'];
    }

    public static function getStoreRules() {

        return ['group_id' => 'required', 'name' => 'required', 'email' => 'required', 'password' => 'required'];
    }

    public static function getUpdateRules() {

        return ['group_id' => 'required', 'name' => 'required', 'email' => 'required'];
    }

    public static function getSex() {

        $sex = [
            'ru' => ['Женский', 'Мужской'],
            'en' => ['Female', 'Male'],
            'es' => ['Hembra', 'Masculino'],
            'fr' => ['Femelle', 'Mâle'],
            'uk' => ['Жіночий', 'Чоловічий']
        ];
        return $sex[\App::getLocale()];
    }
}
