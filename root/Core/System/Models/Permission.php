<?php
namespace STALKER_CMS\Core\System\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Class Permission
 * @package STALKER_CMS\Core\System\Models
 */
class Permission extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'permissions';
    protected $fillable = ['group_id', 'module', 'action', 'status'];
    protected $guarded = [];

    public $timestamps = FALSE;

    public function insert($request) {

        $this->group_id = $request['group_id'];
        $this->module = $request['module'];
        $this->action = $request['action'];
        $this->status = $request['status'];
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::where($id)->first();
        $model->status = $request['status'];
        $model->save();
        return $model;
    }

    public function remove($id) {
        // TODO: Implement remove() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }
}