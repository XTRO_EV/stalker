@section('title') @stop
@section('description') @stop
@section('keywords') @stop
@section('h1') @stop
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
<meta name="keywords" content="@yield('keywords')">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{!!  csrf_token() !!}">
<link rel="shortcut icon" href="{!! asset('core/img/favicon.ico') !!}" type="image/x-icon">
<link rel="icon" href="{!! asset('core/img/favicon.ico') !!}" type="image/x-icon">