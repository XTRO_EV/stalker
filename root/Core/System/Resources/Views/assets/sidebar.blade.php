<?php
namespace STALKER_CMS\Core\System\Resources\Views;

use \STALKER_CMS\Core\System\Http\Controllers\SideBarController;
?>
@if($permissions = \App::make('Permissions'))
    <ul class="main-menu">
        @foreach($permissions as $package_name => $permission)
            @foreach(SideBarController::getSidebarMenu($package_name, $permission) as $name => $module)
                @if($module['permit'])
                    <?php
                    $class = '';
                    $has_children = !empty($module['menu_child']) ? TRUE : FALSE;
                    $menu_children_active = FALSE;
                    if ($has_children):
                        $menu_children_active = SideBarController::menuChildActive($module['menu_child']);
                    endif;
                    ?>
                    <li class="{!! $has_children ? 'sub-menu' : '' !!}{!! $menu_children_active ? ' active toggled' : '' !!}{!! SideBarController::isLinkActive($module['route']) ? ' active c-blue' : '' !!}">
                        <a href="{!! \Route::has($module['route']) ? route($module['route']) : 'javascript:void(0);' !!}">
                            <i class="{{ $module['icon'] }}"> </i>
                            <span class="menu-item-parent">{!! \STALKER_CMS\Vendor\Helpers\array_translate($module['title']) !!}</span>
                        </a>
                        @if($has_children)
                            <ul{!! $menu_children_active ? ' class="show"' : '' !!}>
                                @foreach($module['menu_child'] as $child_name => $child_module)
                                    @if($child_module['permit'])
                                        <?php $link_child_active = SideBarController::isLinkActive($child_module['route']); ?>
                                        <li>
                                            <a href="{{ \Route::has($child_module['route']) ? route($child_module['route']) : 'javascript:void(0);' }}" {!! $link_child_active ? 'class="c-blue"' : '' !!}>
                                                <i class="{{ $child_module['icon'] }} p-r-5"></i>
                                                <span class="menu-item-parent">{!! \STALKER_CMS\Vendor\Helpers\array_translate($child_module['title']) !!}</span>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endif
            @endforeach
        @endforeach
    </ul>
@endif