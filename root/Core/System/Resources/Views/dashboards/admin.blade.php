@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::dashboard.title')</h2>
    </div>
    <div class="mini-charts">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="mini-charts-item bgm-cyan">
                    <div class="clearfix">
                        <div class="count">
                            <small>@lang('core_system_lang::dashboard.count_users')</small>
                            <h2>@numDimensions(\STALKER_CMS\Core\System\Models\User::where('id', '>', 1)->whereActive(TRUE)->count())</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="mini-charts-item bgm-lightgreen">
                    <div class="clearfix">
                        <div class="count">
                            <small>@lang('core_system_lang::dashboard.uploaded_files')</small>
                            @set($fileSizes, \STALKER_CMS\Vendor\Helpers\getDirSize(public_path('uploads')))
                            <h2>@numDimensions(round($fileSizes / 1048576)) Мб</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="mini-charts-item bgm-orange">
                    <div class="clearfix">
                        <div class="count">
                            <small>@lang('core_system_lang::dashboard.installed_modules')</small>
                            <h2>@numDimensions(\STALKER_CMS\Vendor\Models\Packages::whereEnabled(TRUE)->count())</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dash-widgets">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div id="site-visits" class="dash-widget-item bgm-teal">
                    <div class="dash-widget-header">
                        <div class="p-20">
                            <div class="dash-widget-visits"></div>
                        </div>
                        <div class="dash-widget-title">@lang('core_system_lang::dashboard.register_statistic.title')</div>
                    </div>
                    <div class="p-20">
                        <small>@lang('core_system_lang::dashboard.register_statistic.views')</small>
                        <h3 class="m-0 f-400">@numDimensions(0)</h3>
                        <br/>
                        <small>@lang('core_system_lang::dashboard.register_statistic.visitors')</small>
                        <h3 class="m-0 f-400">@numDimensions(0)</h3>
                    </div>
                </div>
            </div>
            @if(\PermissionsController::allowPermission('core_mailer', 'mailer', FALSE))
                <div class="col-md-3 col-sm-6">
                    <div id="site-visits" class="dash-widget-item bgm-lime">
                        <div class="dash-widget-header">
                            <div class="p-20">
                                <div class="dash-widget-emails"></div>
                            </div>
                            <div class="dash-widget-title">@lang('core_system_lang::dashboard.feedback.title')</div>
                        </div>
                        <div class="p-20">
                            <small>@lang('core_system_lang::dashboard.feedback.inbox')</small>
                            <h3 class="m-0 f-400">@numDimensions(\PublicMailer::getSendMessages(TRUE))</h3>
                            <br/>
                            <small>@lang('core_system_lang::dashboard.feedback.views')</small>
                            <h3 class="m-0 f-400">@numDimensions(\PublicMailer::getViewMessages(TRUE))</h3>
                            <br/>
                            <small>@lang('core_system_lang::dashboard.feedback.sends')</small>
                            <h3 class="m-0 f-400">@numDimensions(0)</h3>
                        </div>
                    </div>
                </div>
                @set($notifications, \PublicMailer::getLastMessages())
                @if($notifications->count())
                    <div class="col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-header ch-alt m-b-20">
                                <h2>@lang('core_system_lang::dashboard.feedback_list.title')</h2>
                            </div>
                            <div class="card-body">
                                <div class="listview">
                                    @foreach($notifications as $notification)
                                        <a class="lv-item" href="">
                                            <div class="media">
                                                <div class="pull-left small-image">
                                                    @ProfileAvatar($notification->name, $notification->avatar)
                                                </div>
                                                <div class="media-body">
                                                    <div class="lv-title">{{ $notification->name }}</div>
                                                    <small class="lv-small d-block">{{ \Illuminate\Support\Str::limit($notification->message, 200) }}</small>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                    <a class="lv-footer" href="{{ route('core.mailer.index') }}">
                                        @lang('core_system_lang::dashboard.feedback_list.show_all')
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                @endif
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        BASIC.sparkLine('dash-widget-visits', [], '100%', '95px', 'rgba(255,255,255,0.7)', 'rgba(0,0,0,0)', 2, 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 5, 'rgba(255,255,255,0.4)', '#fff');
        @if(\PermissionsController::allowPermission('core_mailer', 'mailer', FALSE))
            <?php
            $notifications_statistic = [];
            $notifications_month = \STALKER_CMS\Core\Mailer\Models\MailInbox::where('created_at', '>=', \Carbon\Carbon::now()->firstOfMonth())->where('created_at', '<=', \Carbon\Carbon::now())->get();
            $notifications_statistic[\Carbon\Carbon::now()->firstOfMonth()->format('Y-m-d')] = 0;
            foreach($notifications_month as $notification):
                $notifications_statistic[$notification->created_at->format('Y-m-d')] = 0;
            endforeach;
            foreach($notifications_statistic as $date => $count):
                $count_notification = 0;
                foreach($notifications_month as $notification):
                    if($notification->created_at->format('Y-m-d') == $date):
                        $count_notification++;
                    endif;
                endforeach;
                $notifications_statistic[$date] = $count_notification;
            endforeach;
            ksort($notifications_statistic);
            ?>
            BASIC.sparkLine('dash-widget-emails', [{{ implode(', ', $notifications_statistic) }}], '100%', '95px', 'rgba(255,255,255,0.7)', 'rgba(0,0,0,0)', 2, 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 5, 'rgba(255,255,255,0.4)', '#fff');
        //BASIC.sparkBar('dash-widget-emails', [{{ implode(', ', $notifications_statistic) }}], '95px', 4, '#fff', 2);
        @endif
    </script>
@stop