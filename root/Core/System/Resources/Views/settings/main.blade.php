@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.settings.icon') }}"></i> @lang('core_system_lang::settings.main.title')
        </li>
    </ol>
@stop
@section('body-class')
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.settings.icon') }}"></i> @lang('core_system_lang::settings.main.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                <div class="col-sm-3">
                    {!! Form::open(['url' => URL::route('cms.settings.update'), 'class' => 'form-validate', 'id' => 'main-settings-form']) !!}
                    <p class="f-500 m-b-15 c-black">
                        <i class="zmdi zmdi-translate"></i> @lang('core_system_lang::settings.main.interface_language')
                    </p>

                    <div class="form-group">
                        {!! Form::select('APP_LOCALE', $languages, \App::getLocale(), ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('APPLICATION_NAME', config('app.application_name'), ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::settings.main.app_name')</label>
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_system_lang::settings.main.submit')</span>
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop