@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="" class="disabled">
                <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.modules_solutions.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.modules_solutions.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.modules_solutions.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.modules_solutions.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding m-h-250">
            @if($solutions_packages->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="row">
                        @foreach($solutions_packages as $package)
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-header {{ $package['enabled'] ? 'bgm-green' : 'bgm-bluegray' }}">
                                        <h2 class="f-14">{{ \STALKER_CMS\Vendor\Helpers\array_translate($package['package_title']) }}</h2>
                                        @if($package['enabled'])
                                            <ul class="actions">
                                                <li class="dropdown">
                                                    <a href="" data-toggle="dropdown" aria-expanded="true">
                                                        <i class="zmdi zmdi-more-vert c-white"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <a href="" class="disabled">
                                                                @lang('core_system_lang::solutions.settings')
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="" class="disabled">
                                                                @lang('core_system_lang::solutions.check_updates')
                                                            </a>
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <button data-action="{{ route('system.modules.solutions.disable') }}"
                                                                    class="confirm-warning btn-link pull-right c-red p-r-15"
                                                                    autocomplete="off"
                                                                    data-method="delete"
                                                                    data-params="solution={{ $package['package_name'] }}"
                                                                    data-question="@lang('core_system_lang::solutions.disable.question') &laquo;{{ \STALKER_CMS\Vendor\Helpers\array_translate($package['package_title']) }}&raquo;?"
                                                                    data-confirmbuttontext="@lang('core_system_lang::solutions.disable.confirmbuttontext')"
                                                                    data-cancelbuttontext="@lang('core_system_lang::solutions.disable.cancelbuttontext')">
                                                                @lang('core_system_lang::solutions.disable.submit')
                                                            </button>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        @else
                                            <div class="actions">
                                                <a title="@lang('core_system_lang::solutions.install')"
                                                   data-action="{{ route('system.modules.solutions.enable') }}"
                                                   class="confirm-warning solution-install"
                                                   autocomplete="off"
                                                   data-method="post"
                                                   data-params="solution={{ $package['package_name'] }}"
                                                   data-question="@lang('core_system_lang::solutions.enable.question') &laquo;{{ \STALKER_CMS\Vendor\Helpers\array_translate($package['package_title']) }}&raquo;?"
                                                   data-confirmbuttontext="@lang('core_system_lang::solutions.enable.confirmbuttontext')"
                                                   data-cancelbuttontext="@lang('core_system_lang::solutions.enable.cancelbuttontext')">
                                                    <i class="zmdi zmdi-download c-white"></i>
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="card-body card-padding m-h-100 p-t-5">
                                        <small>{{ \STALKER_CMS\Vendor\Helpers\array_translate($package['package_description']) }}</small>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('core_system_lang::solutions.empty')</h2>
            @endif
        </div>
    </div>
@stop