@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li><a href="{{ route('dashboard') }}"><i class="zmdi zmdi-view-dashboard"></i> Дашборд</a></li>
        <li>
            <a href="" class="disabled">
                <i class="{{ config('core_system::menu.icon') }}"></i> {{ config('core_system::menu.title') }}
            </a>
        </li>
        <li>
            <a href="{{ route('system.modules.index') }}">
                <i class="{{ config('core_system::menu.menu_child.modules.icon') }}"></i> {{ config('core_system::menu.menu_child.modules.title') }}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-time-countdown zmdi-hc-fw"></i> @lang('core_system_lang::modules.boot')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-time-countdown zmdi-hc-fw"></i> @lang('core_system_lang::modules.boot')
        </h2>
    </div>
    <div class="card">
        <div class="card-header ch-alt p-40">
            <div id="nestable_list" class="dd margin-left-15">
                <ol class="dd-list">
                    @foreach($packages as $index => $package)
                        <li data-id="{{ $package->id }}" class="dd-item">
                            <div class="dd-handle">
                                <i class="fa {{ config($package->name.'::config.package_icon' ) }}"></i> {{ $package->title }}
                                @if(!empty($package->description))
                                    <p class="note">{{ $package->description }}</p>
                                @endif
                            </div>
                        </li>
                    @endforeach
                </ol>
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    <script>
        $(document).ready(function () {
            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target), output = $(list.data('output'));
                if (window.JSON) {
                    var data = window.JSON.stringify(list.nestable('serialize'));
                    $.ajax({
                        url: "{{ route('system.modules.boot.update') }}",
                        type: "post",
                        data: {data: data, _token: $('meta[name="csrf-token"]').attr('content')},
                        success: function (jhr) {

                        }
                    });
                    output.val(data);
                }
            };

            //$("#nestable_list").nestable({maxDepth: 1, group: 1}).on('change', updateOutput);
        })
    </script>
@stop