<?php

return [
    'cache_settings' => 'Кэшировать настройки',
    'cache_clear' => 'Очистить кэш настроек',
    'php_info' => 'Информация PHP',
    'main' => [
        'title' => 'Настройки',
        'interface_language' => 'Язык интерфейса',
        'app_name' => 'Название приложение',
        'submit' => 'Сохранить'
    ]
];