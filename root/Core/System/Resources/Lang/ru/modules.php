<?php

return [
    'select_all' => 'Выбрать все',
    'boot' => 'Последовательность загрузки',
    'information' => 'Информация',
    'check_updates' => 'Проверить обновления',
    'required' => 'Для установки требуются',
    'disabled' => [
        'question' => 'Отключить модуль',
        'confirmbuttontext' => 'Да, отключить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Отключить',
    ]
];