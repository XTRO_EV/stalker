<?php

return [
    'cache_settings' => 'Cache settings',
    'cache_clear' => 'Clear cache settings',
    'php_info' => 'PHP info',
    'main' => [
        'title' => 'Settings',
        'interface_language' => 'Interface language',
        'app_name' => 'Application name',
        'submit' => 'Save'
    ]
];