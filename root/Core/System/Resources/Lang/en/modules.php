<?php

return [
    'select_all' => 'Select all',
    'boot' => 'Boot Sequence',
    'information' => 'Information',
    'check_updates' => 'Check for updates',
    'required' => 'Required for installation',
    'disabled' => [
        'question' => 'Disable module',
        'confirmbuttontext' => 'Yes, disable',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Disable',
    ]
];