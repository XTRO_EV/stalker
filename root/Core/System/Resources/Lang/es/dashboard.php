<?php

return [
    'control_panel' => 'Panel de control',
    'edit_profile' => 'Editar perfil',
    'edit_profile_permissions' => 'Configuración de acceso',
    'header' => [
        'notification' => 'Notificaciones',
        'settings' => [
            'full_screen' => 'Pantalla completa',
            'edit' => 'Ajustes',
            'logout' => 'Cerrar sesión',
        ],
        'debug_mode' => 'Modo de depuración',
        'services_mode' => 'Modo de mantenimiento',
    ],
    'title' => 'Cuadros de mando',
    'count_users' => 'Usuarios',
    'uploaded_files' => 'Ficheros subidos',
    'installed_modules' => 'Está instalado el módulo',
    'register_statistic' => [
        'title' => 'La inscripción para el mes',
        'views' => 'Vistas',
        'visitors' => 'Visitantes',
    ],
    'feedback' => [
        'title' => 'Retroalimentación',
        'inbox' => 'Recibido',
        'views' => 'Vistas',
        'sends' => 'Las respuestas',
    ],
    'feedback_list' => [
        'title' => 'Retroalimentación',
        'show_all' => 'Lee todo'
    ]
];