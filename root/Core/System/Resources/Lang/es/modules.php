<?php

return [
    'select_all' => 'Seleccionar todo',
    'boot' => 'Secuencia de inicio',
    'information' => 'Información',
    'check_updates' => 'Buscar actualizaciones',
    'required' => 'Necesario para la instalación',
    'disabled' => [
        'question' => 'Módulo desactivar',
        'confirmbuttontext' => 'Sí, deshabilitar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Inhabilitar',
    ]
];