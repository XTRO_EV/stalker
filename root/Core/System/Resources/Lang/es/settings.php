<?php

return [
    'cache_settings' => 'Configuración de la caché',
    'cache_clear' => 'Configuración de la caché borrar',
    'php_info' => 'Información PHP',
    'main' => [
        'title' => 'Ajustes',
        'interface_language' => 'Idioma de interfaz',
        'app_name' => 'Nombre de la aplicación',
        'submit' => 'Guardar'
    ]
];