<?php
namespace STALKER_CMS\Core\Dictionaries\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Dictionary extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'dictionary';
    protected $fillable = [];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->locale = \App::getLocale();
        $this->title = $request::input('title');
        $this->structure = json_encode($request::input('elements'));
        $this->user_id = \Auth::user()->id;
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->user_id = \Auth::id();
        $model->structure = json_encode($request::input('elements'));
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function lists() {

        return $this->hasMany('\STALKER_CMS\Core\Dictionaries\Models\DictionaryLists', 'dictionary_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slug' => 'required', 'title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}