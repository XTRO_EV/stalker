<?php
namespace STALKER_CMS\Core\Dictionaries\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class DictionaryListsFields extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'dictionary_lists_fields';
    protected $fillable = [];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $model = new DictionaryListsFields();
        $model->dictionary_list_id = $request['dictionary_list_id'];
        $model->field = $request['field'];
        $model->value = $request['value'];
        $model->save();
        return $model;
    }

    public function replace($attributes, $value) {

        if (!$model = $this::where($attributes)->first()):
            $model = new DictionaryListsFields();
            $model->dictionary_list_id = $attributes['dictionary_list_id'];
            $model->field = $attributes['field'];
        endif;
        $model->value = $value;
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        return static::where('dictionary_list_id', $id)->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['dictionary_list_id' => 'required'];
    }

    public static function getUpdateRules() {

        return [];
    }
}