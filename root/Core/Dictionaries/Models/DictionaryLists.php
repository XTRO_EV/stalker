<?php
namespace STALKER_CMS\Core\Dictionaries\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class DictionaryLists extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'dictionary_lists';
    protected $fillable = [];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $this->dictionary_id = $request::input('dictionary_id');
        $this->title = $request::input('title');
        $this->order = $this::max('order') + 1;
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        DictionaryListsFields::where('dictionary_list_id', $id)->delete();
        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function fields() {

        return $this->hasMany('\STALKER_CMS\Core\Dictionaries\Models\DictionaryListsFields', 'dictionary_list_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}