<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Core\Dictionaries\Models\DictionaryLists;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

class PublicDictionariesController extends ModuleController {


    public function lists($slug) {

        $lists = new Collection();
        if ($dictionary = Dictionary::whereSlug($slug)->whereLocale(\App::getLocale())->with('lists.fields')->first()):
            foreach ($dictionary->lists as $index => $list):
                $items[$index] = [];
                foreach ($list->fields as $field):
                    $items[$index]['id'] = $list['id'];
                    $items[$index][$field['field']] = $field['value'];
                endforeach;
                $lists->add($items[$index]);
            endforeach;
        endif;
        return $lists;
    }

    public function fields($slug, $id) {

        $items = $this->lists($slug);
        if ($items->count()):
            foreach ($items as $item):
                if ($item['id'] == $id):
                    return $item;
                endif;
            endforeach;
        endif;
        return NULL;
    }
}