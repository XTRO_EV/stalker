<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Core\Dictionaries\Models\DictionaryLists;
use STALKER_CMS\Core\Dictionaries\Models\DictionaryListsFields;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

class DictionaryListsController extends ModuleController implements CrudInterface {

    protected $dictionary;
    protected $model;
    protected $fields;

    public function __construct(Dictionary $dictionary, DictionaryLists $lists, DictionaryListsFields $fields) {

        $this->dictionary = $dictionary->whereLocale(\App::getLocale())->findOrFail(\Request::segment(3));
        $this->model = $lists;
        $this->fields = $fields;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('core_dictionaries', 'dictionaries');
        return view('core_dictionaries_views::lists.index', [
            'dictionary' => $this->dictionary,
            'lists' => $this->model->whereDictionaryId($this->dictionary->id)->orderBy('order')->get()
        ]);
    }

    public function create() {

        \PermissionsController::allowPermission('core_dictionaries', 'create');
        $this->dictionary->structure = json_decode($this->dictionary->structure, TRUE);
        return view('core_dictionaries_views::lists.create', ['dictionary' => $this->dictionary]);
    }

    public function store($dictionary_id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'create');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $list = $this->model->insert($request);
            foreach ($request::input('fields') as $field_name => $field_value):
                $this->fields->insert(['dictionary_list_id' => $list->id, 'field' => $field_name, 'value' => $field_value]);
            endforeach;
            if ($image = $this->uploadImage($request, 'photo')):
                $this->fields->insert(['dictionary_list_id' => $list->id, 'field' => 'photo', 'value' => $image]);
            endif;
            return \ResponseController::success(201)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'edit');
        $item = $this->model->findOrFail($id);
        $fields = $this->fields->whereDictionaryListId($item->id)->lists('value', 'field');
        $structure = [];
        foreach (json_decode($this->dictionary->structure, TRUE) as $index => $element):
            $structure[$index] = $element;
            $structure[$index]['value'] = isset($fields[$element['name']]) ? $fields[$element['name']] : NULL;
        endforeach;
        $this->dictionary->structure = $structure;
        return view('core_dictionaries_views::lists.edit', ['dictionary' => $this->dictionary, 'item' => $item]);
    }

    public function update($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'edit');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            foreach ($request::input('fields') as $field_name => $field_value):
                $this->fields->replace(['dictionary_list_id' => $id, 'field' => $field_name], $field_value);
            endforeach;
            if ($image = $this->uploadImage($request, 'photo')):
                $this->deleteImage($id, 'photo');
                $this->fields->replace(['dictionary_list_id' => $id, 'field' => 'photo'], $image);
            endif;
            return \ResponseController::success(202)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'delete');
        $request = \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
    }

    /**************************************************************************************************************/

    private function uploadImage(\Request $request, $field) {

        if ($photo = Helpers\trim_base64_image($request::input($field))):
            $upload_directory = 'dictionaries';
            $fileName = time() . "_" . rand(1000, 1999) . '.jpg';
            $photoPath = Helpers\double_slash($upload_directory . '/' . $fileName);
            \Storage::put($photoPath, base64_decode($photo));
            return Helpers\add_first_slash($photoPath);
        else:
            return NULL;
        endif;
    }

    private function deleteImage($dictionary_list_id, $field) {

        if ($ListsField = $this->fields->whereDictionaryListId($dictionary_list_id)->whereField($field)->first()):
            if (!empty($ListsField->value) && \Storage::exists($ListsField->value)):
                \Storage::delete($ListsField->value);
                $ListsField->value = NULL;
            endif;
            $ListsField->save();
        endif;
    }
}