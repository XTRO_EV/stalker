<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

class DictionariesController extends ModuleController implements CrudInterface {

    protected $model;

    public function __construct(Dictionary $dictionary) {

        $this->model = $dictionary;
        $this->middleware('auth');
        \PermissionsController::allowPermission('core_dictionaries', 'constructor');
    }

    public function index() {

        return view('core_dictionaries_views::dictionaries.index', [
            'dictionaries' => $this->model->orderBy('updated_at', 'DESC')->with('lists')->get()
        ]);
    }

    public function create() {

        return view('core_dictionaries_views::dictionaries.create', [
            'information_items' => $this->getInformationItems()
        ]);
    }

    public function store() {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $dictionary = $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.dictionaries.lists_index', $dictionary->id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {

        return view('core_dictionaries_views::dictionaries.edit', [
            'information_items' => $this->getInformationItems(),
            'dictionary' => $this->model->findOrFail($id)
        ]);
    }

    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.dictionaries.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($id) {

        $request = \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.dictionaries.index'))->json();
    }
}