<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::text('fields['.$element['name'].']', isset($element['value']) ? $element['value'] : NULL, ['class'=>'input-sm form-control fg-input']) !!}
    </div>
    <label class="fg-label">{{ $element['placeholder'] }}</label>
</div>