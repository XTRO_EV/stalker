@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('core.dictionaries.index') }}">
                <i class="{{ config('core_dictionaries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_dictionaries::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="{{ config('core_dictionaries::menu.menu_child.dictionaries.icon') }}"></i> {{ $dictionary->title }}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_dictionaries::menu.menu_child.dictionaries.icon') }}"></i> {{ $dictionary->title }}
        </h2>
    </div>
    @BtnAdd('core.dictionaries.lists_create', $dictionary->id)
    <div class="card">
        <div class="card-body card-padding m-h-250">
            @if($lists->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        @foreach($lists as $lists)
                            <div class="js-item-container lv-item media">
                                <div class="media-body">
                                    <div class="lv-title">{{ $lists->title }}</div>
                                    <ul class="lv-attrs">
                                        <li>ID: @numDimensions($lists->id)</li>
                                    </ul>
                                    <div class="lv-actions actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="{{ route('core.dictionaries.lists_edit', [$dictionary->id, $lists->id]) }}">
                                                    @lang('core_dictionaries_lang::lists.edit')
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                {!! Form::open(['route' => ['core.dictionaries.lists_destroy', $dictionary->id, $lists->id], 'method' => 'DELETE']) !!}
                                                <button type="submit"
                                                        class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                        autocomplete="off"
                                                        data-question="@lang('core_dictionaries_lang::lists.delete.question') &laquo;{{ $dictionary->title }}&raquo;?"
                                                        data-confirmbuttontext="@lang('core_dictionaries_lang::lists.delete.confirmbuttontext')"
                                                        data-cancelbuttontext="@lang('core_dictionaries_lang::lists.delete.cancelbuttontext')">
                                                    @lang('core_dictionaries_lang::lists.delete.submit')
                                                </button>
                                                {!! Form::close() !!}
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('core_dictionaries_lang::dictionaries.empty')</h2>
            @endif
        </div>
    </div>
@stop