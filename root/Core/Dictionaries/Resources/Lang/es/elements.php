<?php

return [
    'text' => [
        'title' => 'Сampo de texto',
        'name' => 'El nombre del campo de texto [nombre de atributo]',
        'placeholder' => 'Descripción del campo de formulario'
    ],
    'textarea' => [
        'title' => 'Area de texto',
        'name' => 'El nombre del campo de texto [nombre de atributo]',
        'placeholder' => 'Descripción del campo de formulario',
        'redactor' => 'Utilice el editor visual'
    ],
    'select' => [
        'title' => 'la lista desplegable',
        'name' => 'Nombre de la lista desplegable [nombre de atributo]',
        'placeholder' => 'Descripción lista desplegable',
        'data_item' => 'lista de información'
    ],
    'checkbox' => [
        'title' => 'Bandera',
        'name' => 'Nombre casilla [nombre de atributo]',
        'placeholder' => 'Descripción de la bandera'
    ],
    'file' => [
        'title' => 'Campo de descarga de archivos',
        'name' => 'Nombre del campo [nombre de atributo]',
        'placeholder' => 'Descripción del campo'
    ],
    'image' => [
        'title' => 'Campo de la carga de imágenes',
        'name' => 'Nombre del campo [nombre de atributo]',
        'placeholder' => 'Descripción del campo'
    ],
    'datapicker' => [
        'title' => 'Campo para la introducción de la fecha',
        'name' => 'Nombre del campo [nombre de atributo]',
        'placeholder' => 'Descripción del campo'
    ]
];