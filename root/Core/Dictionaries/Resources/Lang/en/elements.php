<?php

return [
    'text' => [
        'title' => 'Text field',
        'name' => 'The name of the text field [attribute name]',
        'placeholder' => 'Description form field'
    ],
    'textarea' => [
        'title' => 'Text area',
        'name' => 'The name of the text field [attribute name]',
        'placeholder' => 'Description form field',
        'redactor' => 'Use the visual editor'
    ],
    'select' => [
        'title' => 'Drop-down list',
        'name' => 'Name drop-down list [attribute name]',
        'placeholder' => 'Description drop-down list',
        'data_item' => 'Information list'
    ],
    'checkbox' => [
        'title' => 'Checkbox',
        'name' => 'Checkbox name [attribute name]',
        'placeholder' => 'Description of the checkbox'
    ],
    'file' => [
        'title' => 'Field download file',
        'name' => 'Field name [attribute name]',
        'placeholder' => 'Description form field'
    ],
    'image' => [
        'title' => 'Field loading image',
        'name' => 'Field name [attribute name]',
        'placeholder' => 'Description form field'
    ],
    'datapicker' => [
        'title' => 'Field to enter the date',
        'name' => 'Field name [attribute name]',
        'placeholder' => 'Description form field'
    ]
];