<?php
namespace STALKER_CMS\Core\Dictionaries\Facades;

use Illuminate\Support\Facades\Facade;

class Dictionary extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicDictionariesController';
    }
}