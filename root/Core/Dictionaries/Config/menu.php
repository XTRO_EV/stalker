<?php

return [
    'package' => 'core_dictionaries',
    'title' => ['ru' => 'Информационные списки', 'en' => 'Information lists', 'es' => 'Listas de información'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-collection-bookmark',
    'menu_child' => [
        'dictionaries' => [
            'title' => ['ru' => 'Словари', 'en' => 'Dictionaries', 'es' => 'Diccionarios'],
            'route' => 'core.dictionaries.index',
            'icon' => 'zmdi zmdi-check-circle-u'
        ]
    ]
];