<?php

return [
    'package_name' => 'core_dictionaries',
    'package_title' => ['ru' => 'Модуль словарей', 'en' => 'Module dictionaries', 'es' => 'Módulo diccionarios'],
    'package_icon' => 'zmdi zmdi-collection-bookmark',
    'version' => [
        'ver' => 0.2,
        'date' => '01.03.2016'
    ],
    'form_elements' => [
        'text' => Lang::get('core_dictionaries_lang::elements.text.title'),
        'textarea' => Lang::get('core_dictionaries_lang::elements.textarea.title'),
        'select' => Lang::get('core_dictionaries_lang::elements.select.title'),
        'checkbox' => Lang::get('core_dictionaries_lang::elements.checkbox.title'),
        'file' => Lang::get('core_dictionaries_lang::elements.file.title'),
        'image' => Lang::get('core_dictionaries_lang::elements.image.title'),
        'datapicker' => Lang::get('core_dictionaries_lang::elements.datapicker.title'),
    ]
];