<?php

namespace  STALKER_CMS\Core\Dictionaries\Providers;

use STALKER_CMS\Core\Dictionaries\Http\Controllers\PublicDictionariesController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_dictionaries_views');
        $this->registerLocalization('core_dictionaries_lang');
        $this->registerConfig('core_dictionaries::config', 'Config/dictionaries.php');
        $this->registerSettings('core_dictionaries::settings', 'Config/settings.php');
        $this->registerActions('core_dictionaries::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_dictionaries::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
    }

    public function register() {

        \App::bind('PublicDictionariesController', function () {
            return new PublicDictionariesController();
        });
    }

    /********************************************************************************************************************/
    protected function registerBladeDirectives() {

    }
}
