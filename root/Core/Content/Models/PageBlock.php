<?php
namespace STALKER_CMS\Core\Content\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class PageBlock extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'content_pages_blocks';
    protected $fillable = ['slug', 'page_id', 'template_id', 'content', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->page_id = $request::input('page_id');
        $this->title = $request::input('title');
        $this->template_id = $request::input('template_id');
        $this->content = $request::input('content');
        $this->user_id = \Auth::user()->id;
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->template_id = $request::input('template_id');
        $model->title = $request::input('title');
        $model->content = $request::input('content');
        $model->user_id = \Auth::user()->id;
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function template() {

        return $this->hasOne('\STALKER_CMS\Core\Content\Models\PageTemplate', 'id', 'template_id');
    }

    public function page() {

        return $this->belongsTo('\STALKER_CMS\Core\Content\Models\Page', 'page_id', 'id');
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slug' => 'required', 'page_id' => 'required'];
    }

    public static function getUpdateRules() {

        return ['page_id' => 'required'];
    }
}