<?php
namespace STALKER_CMS\Core\Content\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class PageTemplate extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'content_pages_templates';
    protected $fillable = ['menu_type', 'locale', 'title', 'path', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $this->menu_type = $request::input('template_type');
        $this->locale = \App::getLocale();
        $this->title = $request::input('title');
        $this->path = $request::input('path');
        $this->required = $request::input('required');
        $this->user_id = \Auth::user()->id;
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->user_id = \Auth::user()->id;
        $model->save();
        return $model;
    }

    public function remove($id) {

        Page::whereTemplateId($id)->update(['template_id' => NULL]);
        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function pages() {

    }

    public function menus() {

        return $this->hasMany('\STALKER_CMS\Core\Content\Models\Menu', 'template_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['title' => 'required', 'path' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}