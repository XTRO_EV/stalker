<?php
namespace STALKER_CMS\Core\Content\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Menu extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'content_menu';
    protected $fillable = ['slug', 'locale', 'template_id', 'title', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $model = new Menu();
        $model->slug = $request::input('slug');
        $model->locale = \App::getLocale();
        $model->template_id = $request::input('template_id');
        $model->title = $request::input('title');
        $model->user_id = \Auth::user()->id;
        $model->save();
        return $model;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->template_id = $request::input('template_id');
        $model->user_id = \Auth::user()->id;
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function template() {

        return $this->hasOne('\STALKER_CMS\Core\Content\Models\PageTemplate', 'id', 'template_id');
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function items() {

        return $this->hasMany('\STALKER_CMS\Core\Content\Models\MenuItem', 'menu_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slug' => 'required', 'template_id' => 'required'];
    }

    public static function getUpdateRules() {

        return ['template_id' => 'required'];
    }
}