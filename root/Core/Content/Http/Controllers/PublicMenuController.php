<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;


use Illuminate\Support\Collection;
use STALKER_CMS\Core\Content\Models\Menu;
use STALKER_CMS\Core\Content\Models\MenuItem;
use STALKER_CMS\Core\Content\Models\PageTemplate;

class PublicMenuController extends ModuleController {

    public function make($slug) {

        $menu_items = $items = [];
        if (Menu::where(['locale' => \App::getLocale(), 'slug' => $slug])->exists()):
            $menu_items_list = Menu::where(['locale' => \App::getLocale(), 'slug' => $slug])->with('items.page', 'items.file')->first();
            foreach ($menu_items_list->items as $item):
                $items[$item->parent_id][] = $item->toArray();
            endforeach;
            $menu_items = \STALKER_CMS\Vendor\Helpers\recursive($items, 0);
        endif;
        return $menu_items;
    }

    public function getTemplate($slug) {

        if ($menu = Menu::where(['locale' => \App::getLocale(), 'slug' => $slug])->first()):
            $template = PageTemplate::whereFind(['menu_type' => 'menu', 'locale' => \App::getLocale(), 'id' => $menu->template_id]);
            return \PublicPage::getViewPath($template);
        else:
            return NULL;
        endif;
    }

    public function listItems($menu_id, $exclude_id = NULL) {

        $items = new Collection();
        $items->put(NULL, \Lang::get('core_content_lang::menu_items.top_item'));
        foreach (MenuItem::whereMenuId($menu_id)->lists('title', 'id') as $item_id => $item_title):
            if (!is_null($exclude_id) && $item_id == $exclude_id):
                continue;
            endif;
            $items->put($item_id, $item_title);
        endforeach;
        return $items;
    }
}