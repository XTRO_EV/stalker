<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\View;
use STALKER_CMS\Core\Content\Models\PageTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

class TemplatesController extends ModuleController implements CrudInterface {

    protected $model;

    public function __construct(PageTemplate $pageTemplate) {

        $this->model = $pageTemplate;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('core_content', 'templates');
        $templates = [];
        foreach (config('core_content::config.menu_types') as $menu_type_slug => $menu_type):
            $templates[$menu_type_slug] = [
                'title' => Helpers\array_translate($menu_type),
                'path' => Helpers\array_translate(Helpers\settings(['core_content', 'pages', 'templates_dir'], 'note')),
                'files' => $this->model->whereLocale(\App::getLocale())->whereMenuType($menu_type_slug)->orderBy('updated_at', 'DESC')->get()
            ];
        endforeach;
        if (\Request::has('type') && isset($templates[\Request::get('type')])):
            $templates_type = array_first($templates, function ($key, $value) {
                return $key >= \Request::get('type');
            });
            $templates = [
                \Request::get('type') => $templates_type
            ];
        endif;
        return view('core_content_views::templates.index', compact('templates'));
    }

    public function create() {

        \PermissionsController::allowPermission('core_content', 'templates');
        $menu_types = [];
        foreach (config('core_content::config.menu_types') as $menu_type_slug => $menu_type):
            $menu_types[$menu_type_slug] = Helpers\array_translate($menu_type);
        endforeach;
        $template_content = '';
        if (view()->exists('core_content_views::templates.sketch')):
            $template_content = \File::get(view('core_content_views::templates.sketch')->getPath());
        endif;
        return view('core_content_views::templates.create', compact('menu_types', 'template_content'));
    }

    public function store() {

        \PermissionsController::allowPermission('core_content', 'templates');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $view_path = base_path('home/Resources/Views/' . Helpers\settings(['core_content', 'pages', 'templates_dir']) . '/' . $request::input('path') . '.blade.php');
            $view_path = Helpers\double_slash($view_path);
            if (\File::exists($view_path) === FALSE):
                $request::merge(['path' => $request::input('path') . '.blade.php', 'required' => FALSE, 'locale' => \App::getLocale()]);
                $this->model->insert($request);
                \File::put($view_path, $request::input('content'));
                \Artisan::call('CacheKiller');
                return \ResponseController::success(1600)->redirect(route('core.content.templates.index'))->json();
            else:
                return \ResponseController::error(2610)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {

        \PermissionsController::allowPermission('core_content', 'templates');
        $template_content = '';
        $template = $this->model->findOrFail($id);
        $view_path = base_path('/home/Resources/Views/' . Helpers\settings(['core_content', 'pages', 'templates_dir']) . '/' . $template->path);
        if (\File::exists(realpath($view_path))):
            $template_content = \File::get(realpath($view_path));
        endif;
        return view('core_content_views::templates.edit', compact('template_content', 'template'));
    }

    public function update($id) {

        \PermissionsController::allowPermission('core_content', 'templates');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $template = $this->model->findOrFail($id);
            $view_path = base_path('/home/Resources/Views/' . Helpers\settings(['core_content', 'pages', 'templates_dir']) . '/' . $template->path);
            $view_path = Helpers\double_slash($view_path);
            \File::put($view_path, $request::input('content'));
            $this->model->replace($id, $request);
            \Artisan::call('CacheKiller');
            return \ResponseController::success(202)->redirect(route('core.content.templates.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($id) {

        \PermissionsController::allowPermission('core_content', 'templates');
        $request = \RequestController::isAJAX()->init();
        try {
            $template = $this->model->findOrFail($id);
            $view_path = base_path('/home/Resources/Views/' . Helpers\settings(['core_content', 'pages', 'templates_dir']) . '/' . $template->path);
            if (\File::exists(realpath($view_path))):
                \File::delete(realpath($view_path));
            endif;
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('core.content.templates.index'))->json();
        } catch (Exception $e) {
            return \ResponseController::success(2503)->json();
        }
    }
}