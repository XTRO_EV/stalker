<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use Illuminate\Support\Collection;
use STALKER_CMS\Core\Content\Models\Menu;
use STALKER_CMS\Core\Content\Models\MenuItem;
use STALKER_CMS\Core\Content\Models\Page;
use STALKER_CMS\Core\Content\Models\PageTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

use STALKER_CMS\Vendor\Helpers as Helpers;

class MenuItemController extends ModuleController implements CrudInterface {

    protected $menu;
    protected $model;

    public function __construct(MenuItem $item, Menu $menu) {

        $this->menu = $menu->whereLocale(\App::getLocale())->findOrFail(\Request::segment(4));
        $this->model = $item;
        $this->middleware('auth');
        \PermissionsController::allowPermission('core_content', 'menu');
    }

    public function index() {

        $items = \PublicMenu::make($this->menu->slug);
        return view('core_content_views::menu.items.index', [
            'menu' => $this->menu,
            'items' => $items,
            'list' => $this->makeMenuList(\PublicMenu::make($this->menu->slug))
        ]);
    }

    public function create() {

        return view('core_content_views::menu.items.create', ['menu' => $this->menu]);
    }

    public function store($menu_id = NULL) {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.content.menu.items_index', $menu_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($menu_id = NULL, $id = NULL) {

        return view('core_content_views::menu.items.edit', [
            'menu' => $this->menu,
            'item' => $this->model->findOrFail($id)
        ]);
    }

    public function update($menu_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_content', 'menu');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.content.menu.items_index', $menu_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($menu_id = NULL, $id = NULL) {

        $request = \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.content.menu.items_index', $menu_id))->json();
    }

    /*****************************************************************************************/

    private function makeMenuList(array $items) {

        $tree = '';
        $types = [
            'page_link' => \Lang::get('core_content_lang::menu_items.insert.form.page_link_type'),
            'external_link' => \Lang::get('core_content_lang::menu_items.insert.form.external_link'),
            'anchor_link' => \Lang::get('core_content_lang::menu_items.insert.form.anchor_link'),
            'file_link' => \Lang::get('core_content_lang::menu_items.insert.form.file_link')
        ];
        $tree .= '<ul>';
        foreach ($items as $item) :
            $tree .= view('core_content_views::menu.items.item', ['element' => $item, 'types' => $types, 'menu' => $this->menu])->render();
            if (isset($item['sub_menu'])):
                $tree .= $this->makeMenuList($item['sub_menu']);
            endif;
        endforeach;
        $tree .= '</ul>';
        return $tree;
    }
}