<?php

namespace STALKER_CMS\Core\Content\Http\Middleware;

use Closure;

class localesMiddleware {

    public function handle($request, Closure $next, $package = NULL, $module = NULL, $setting = NULL) {

        \App::setLocale(\PublicPage::getLocale());
        return $next($request);
    }
}