@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="" class="disabled">
                <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.menu.title')) !!}
        </h2>
    </div>
    @BtnAdd('core.content.menu.create')
    <div class="card">
        <div class="card-body card-padding m-h-250">
            <div class="row">
                @if($menus->count())
                    @foreach($menus as $index => $menu)
                        <div class="js-item-container col-sm-3">
                            <div class="card">
                                <div class="card-header bgm-green">
                                    <h2>{{ $menu->title }}</h2>
                                    <ul class="actions actions-alt">
                                        <li class="dropdown">
                                            <a aria-expanded="false" data-toggle="dropdown" href="">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if(\PermissionsController::allowPermission('core_content', 'edit', FALSE))
                                                    <li>
                                                        <a href="{{ route('core.content.menu.items_index', $menu->id) }}">
                                                            @lang('core_content_lang::menu.items')
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('core.content.menu.edit', $menu->id) }}">
                                                            @lang('core_content_lang::menu.edit')
                                                        </a>
                                                    </li>
                                                @endif
                                                <li>
                                                    <a href="javascript:void(0);" class="js-copy-link"
                                                       data-clipboard-text="{{ '@'.'Menu(\'' . $menu->slug . '\')' }}">
                                                        @lang('core_content_lang::menu.embed')
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    {!! Form::open(['route' => ['core.content.menu.destroy', $menu->id], 'method' => 'DELETE']) !!}
                                                    <button type="submit"
                                                            class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                            autocomplete="off"
                                                            data-question="@lang('core_content_lang::menu.delete.question') &laquo;{{ $menu->title }}&raquo;?"
                                                            data-confirmbuttontext="@lang('core_content_lang::menu.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('core_content_lang::menu.delete.cancelbuttontext')">
                                                        @lang('core_content_lang::menu.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body card-padding p-10 m-h-100">
                                    <ul class="clist clist-angle">
                                        <li>ID: @numDimensions($menu->id)</li>
                                        <li>
                                            @lang('core_content_lang::menu.symbolic_code'):
                                            {{ $menu->slug }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                @else
                    <h2 class="f-16 c-gray">@lang('core_content_lang::menu.empty')</h2>
                @endif
            </div>
        </div>
    </div>
@stop