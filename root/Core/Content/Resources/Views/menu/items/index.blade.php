@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="" class="disabled">
                <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
            </a>
        </li>
        <li>
            <a href="{{ route('core.content.menu.index') }}">
                <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-receipt"></i> @lang('core_content_lang::menu_items.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-receipt"></i> @lang('core_content_lang::menu_items.breadcrumb')</h2>
    </div>
    @BtnAdd('core.content.menu.items_create', $menu->id)
    <div class="card">
        <div class="card-body card-padding m-h-250">
            <div class="row">
                @if(count($items))
                    <div class="listview lv-bordered lv-lg">
                        <div class="lv-body first-ul-pad-0">
                            {!! $list !!}
                        </div>
                    </div>
                @else
                    <h2 class="f-16 c-gray">@lang('core_content_lang::menu.empty')</h2>
                @endif
            </div>
        </div>
    </div>
@stop