@set($menu_items, \PublicMenu::make($menu_slug))
@if(count($menu_items))
    <ul class="bottom-menu-ul">
        @foreach($menu_items as $menu_level_1)
            <li class="bottom-menu-li">
                @if(isset($menu_level_1['level_2']) && count($menu_level_1['level_2']))
                    <a {!! !empty($menu_level_1['menu_attributes']) ? $menu_level_1['menu_attributes'] : 'class="bottom-menu-link"' !!} href="javascript:void(0);">
                        {{ $menu_level_1['menu_title'] }}
                    </a>
                    <ul class="bottom-sub-menu-ul">
                        @foreach($menu_level_1['level_2'] as $menu_level_2)
                            <li class="bottom-sub-menu-li">
                                <a {!! !empty($menu_level_2['menu_attributes']) ? $menu_level_2['menu_attributes'] : 'class="bottom-menu-link"' !!} href="{{ route('public.page.' . $menu_level_2['page_slug']) }}">
                                    {{ $menu_level_2['menu_title'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <a {!! !empty($menu_level_1['menu_attributes']) ? $menu_level_1['menu_attributes'] : 'class="bottom-menu-link"' !!} href="{{ route('public.page.' . $menu_level_1['page_slug']) }}">
                        {{ $menu_level_1['menu_title'] }}
                    </a>
                @endif
            </li>
        @endforeach
    </ul>
@endif