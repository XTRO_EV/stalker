<?php

return [
    'pages' => [
        'title' => ['ru' => 'Просмотр', 'en' => 'View', 'es' => 'Ver'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-eye'
    ],
    'create' => [
        'title' => ['ru' => 'Создание', 'en' => 'Add', 'es' => 'Añadir'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-collection-plus'
    ],
    'edit' => [
        'title' => ['ru' => 'Редактирование', 'en' => 'Edit', 'es' => 'Edición'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-edit'
    ],
    'delete' => [
        'title' => ['ru' => 'Удаление', 'en' => 'Delete', 'es' => 'Eliminar'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-delete'
    ],
    'languages' => [
        'title' => ['ru' => 'Локализация', 'en' => 'Localization', 'es' => 'localización'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-translate'
    ],
    'menu' => [
        'title' => ['ru' => 'Конструктор меню', 'en' => 'Menu designer', 'es' => 'Diseñador de menús'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-menu'
    ],
    'templates' => [
        'title' => ['ru' => 'Работа с шаблонами', 'en' => 'Working with templates', 'es' => 'Trabajar con plantillas'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-layers'
    ],
    'seo' => [
        'title' => ['ru' => 'Поисковая оптимизация', 'en' => 'Search Engine Optimization', 'es' => 'Search Engine Optimization'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-globe'
    ],
    'open_graph' => [
        'title' => ['ru' => 'Open Graph', 'en' => 'Open Graph', 'es' => 'Open Graph'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-share'
    ]
];