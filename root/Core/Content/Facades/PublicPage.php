<?php
namespace STALKER_CMS\Core\Content\Facades;

use Illuminate\Support\Facades\Facade;

class PublicPage extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicPagesController';
    }
}