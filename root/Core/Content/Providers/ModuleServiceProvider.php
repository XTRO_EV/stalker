<?php

namespace STALKER_CMS\Core\Content\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Core\Content\Http\Controllers\PublicMenuController;
use STALKER_CMS\Core\Content\Http\Controllers\PublicPagesController;
use STALKER_CMS\Vendor\Models\Languages;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider {

    public function boot() {

        $this->setPath(base_path('home'));
        $this->registerViews('site_views');
        $this->registerLocalization('site_lang');
        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_content_views');
        $this->registerLocalization('core_content_lang');
        $this->registerConfig('core_content::config', 'Config/content.php');
        $this->registerSettings('core_content::settings', 'Config/settings.php');
        $this->registerActions('core_content::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_content::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
        $this->publishesTemplates();
        $this->publishesAssets();
        $this->publishesElixir();
    }

    public function register() {

        \App::bind('PublicPagesController', function () {
            return new PublicPagesController();
        });
        \App::bind('PublicMenuController', function () {
            return new PublicMenuController();
        });
        $this->app->singleton('Locales', function ($app) {
            $locales = Languages::whereActive(TRUE)->pluck('slug', 'slug')->toArray();
            $locales[\App::getLocale()] = '';
            return $locales;
        });
    }

    /********************************************************************************************************************/
    public function registerBladeDirectives() {

        \Blade::directive('metaTitle', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (empty($expression)):
                return "<?php \$__env->startSection('title', @\$page->page_title); ?>";
            else:
                return "<?php \$__env->startSection('title', $expression); ?>";
            endif;
        });

        \Blade::directive('metaDescription', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (empty($expression)):
                return "<?php \$__env->startSection('description', @\$page->page_description); ?>";
            else:
                return "<?php \$__env->startSection('description', $expression); ?>";
            endif;
        });

        \Blade::directive('PageTitle', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (empty($expression)):
                return "<?php echo @\$page->page_h1; ?>";
            else:
                return "<?php echo $expression; ?>";
            endif;
        });

        \Blade::directive('anchor', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                $expressions = [];
                foreach (explode(',', $expression) as $parameter):
                    $expressions[] = trim($parameter);
                endforeach;
                switch (count($expressions)):
                    case 1:
                        return '<a href="<?php echo route(\'public.page.\' . ' . $expressions[0] . ');?>"><?php echo ' . $expressions[0] . '; ?></a>';
                    case 2:
                        return '<a href="<?php echo route(\'public.page.\' . ' . $expressions[0] . ');?>"><?php echo ' . $expressions[1] . '; ?></a>';
                    case 3:
                        return '<a class="<?php echo ' . $expressions[2] . '?>" href="<?php echo route(\'public.page.\' . ' . $expressions[0] . ');?>"><?php echo ' . $expressions[1] . '; ?></a>';
                    case 4:
                        return '<a target="_blank" class="<?php echo ' . $expressions[2] . '?>" href="<?php echo route(\'public.page.\' . ' . $expressions[0] . ');?>"><?php echo ' . $expressions[1] . '; ?></a>';
                endswitch;
            endif;
            return NULL;
        });

        \Blade::directive('Menu', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                $expressions = [];
                foreach (explode(',', $expression, 2) as $parameter):
                    $expressions[] = preg_replace("/[\']/i", '', trim($parameter));
                endforeach;
                switch (count($expressions)):
                    case 1:
                        $template = \PublicMenu::getTemplate($expressions[0]);
                        if (!is_null($template) && view()->exists("site_views::$template")):
                            return "<?php echo \$__env->make('site_views::$template', ['menu_slug' => '$expressions[0]'])->render(); ?>";
                        endif;
                        break;
                    case 2:
                        if (view()->exists("site_views::$expressions[1]")):
                            return "<?php echo \$__env->make('site_views::$expressions[1]', ['menu_slug' => '$expressions[0]'])->render(); ?>";
                        endif;
                        break;
                endswitch;
            endif;
            return NULL;
        });

        \Blade::directive('PageBlock', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                $Block = '<?php if(isset($blocks[' . $expression . '])): ?>';
                $Block .= '<?php echo $blocks[' . $expression . ']; ?>';
                $Block .= '<?php endif; ?>';
                return $Block;
            endif;
            return NULL;
        });
    }

    public function publishesTemplates() {

        $this->publishes([
            __DIR__ . '/../Resources/Templates' => base_path('home/Resources'),
        ]);
    }

    public function publishesAssets() {

        $this->publishes([
            __DIR__ . '/../Resources/Assets' => public_path('packages/content'),
        ]);
    }

    public function publishesElixir() {

        $this->publishes([
            __DIR__ . '/../Resources/Elixir' => public_path('theme'),
        ]);
    }
}