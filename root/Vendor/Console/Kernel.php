<?php

namespace STALKER_CMS\Vendor\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    protected $commands = [
        Commands\Uninstall::class,
        Commands\Install::class,
        Commands\CacheKiller::class
    ];

    protected function schedule(Schedule $schedule) {

    }
}
