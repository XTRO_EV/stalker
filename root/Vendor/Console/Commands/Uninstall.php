<?php

namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;
use League\Flysystem\Exception;

class Uninstall extends Command {

    protected $signature = 'Uninstall';

    protected $description = 'Отмена установки';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        if ($this->confirm('Отмена установки. Продолжить? [yes|no]')) {
            $tables = \DB::select('show full tables where Table_Type != "VIEW"');
            $views = \DB::select('show full tables where Table_Type = "VIEW"');
            if (count($tables)):
                foreach ($tables as $table):
                    if (is_object($table) && isset($table->Tables_in_cms)):
                        \Schema::drop($table->Tables_in_cms);
                    endif;
                endforeach;
            endif;
            if (count($views)):
                foreach ($views as $view):
                    if (is_object($view) && isset($view->Tables_in_cms)):
                        \DB::statement('DROP VIEW ' . $view->Tables_in_cms);
                    endif;
                endforeach;
            endif;
            if (file_exists(base_path('.env'))):
                unlink(base_path('.env'));
            endif;
            if (file_exists(base_path('bootstrap/cache/config.php'))):
                unlink(base_path('bootstrap/cache/config.php'));
            endif;
            if (file_exists(base_path('bootstrap/cache/services.php'))):
                unlink(base_path('bootstrap/cache/services.php'));
            endif;
            if (file_exists(storage_path('logs/laravel.log'))):
                unlink(storage_path('logs/laravel.log'));
            endif;

            $cachedViewsDirectory = storage_path('framework/views');
            array_map("unlink", glob("$cachedViewsDirectory/*.php"));

            setcookie ("stalker_cms", "", time() - 3600);
            $this->info('Отмена установки выполнена успешно');
        }
    }
}
