<?php

namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;
use League\Flysystem\Exception;

class Install extends Command {

    protected $signature = 'Install';

    protected $description = 'Тихая установка';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        if ($this->confirm('Установка CMS. Продолжить? [yes|no]')) {
            if (file_exists(base_path('.env'))):
                $this->error('Установка отменена. CMS уже установлена.');
                return FALSE;
            endif;
        }
    }
}
