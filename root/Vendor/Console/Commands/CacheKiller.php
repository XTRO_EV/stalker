<?php

namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;
use League\Flysystem\Exception;

class CacheKiller extends Command {

    protected $signature = 'CacheKiller';

    protected $description = 'Очистка кэша приложения';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $cachedViewsDirectory = storage_path('framework/views');
        array_map("unlink", glob("$cachedViewsDirectory/*.php"));
    }
}
