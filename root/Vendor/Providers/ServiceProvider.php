<?php
namespace STALKER_CMS\Vendor\Providers;

use Illuminate\Support\ServiceProvider as MainServiceProvider;

abstract class ServiceProvider extends MainServiceProvider {

    protected $RealPath;

    protected function setPath($path) {

        $this->RealPath = realpath($path);
    }

    /********************************************************************************************************************/
    protected function registerViews($namespace) {

        $this->loadViewsFrom($this->RealPath . '/Resources/Views', $namespace);
    }

    protected function registerLocalization($namespace) {

        $this->loadTranslationsFrom($this->RealPath . '/Resources/Lang', $namespace);
    }

    protected function registerBladeDirectives() {

    }

    protected function registerConfig($namespace, $config_file) {

        $this->setConfigurationFile($namespace, $config_file);
    }

    protected function registerSettings($namespace, $settings_file) {

        $this->setConfigurationFile($namespace, $settings_file);
    }

    protected function registerActions($namespace, $actions_file) {

        $this->setConfigurationFile($namespace, $actions_file);
    }

    protected function registerSystemMenu($namespace, $menu_file) {

        $this->setConfigurationFile($namespace, $menu_file);
    }

    /********************************************************************************************************************/
    protected function setConfigurationFile($namespace, $file) {

        $packageConfigFile = $this->RealPath . '/' . $file;
        $config = $this->app['files']->getRequire($packageConfigFile);
        $this->app['config']->set($namespace, $config);
    }
}