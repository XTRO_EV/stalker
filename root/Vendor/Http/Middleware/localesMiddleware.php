<?php

namespace STALKER_CMS\Vendor\Http\Middleware;

use Closure;

class localesMiddleware {

    public function handle($request, Closure $next, $package = NULL, $module = NULL, $setting = NULL) {

        if (\Auth::guard()->check()):
            \App::setLocale(\Auth::user()->locale);
        endif;
        return $next($request);
    }
}