<?php

namespace STALKER_CMS\Vendor\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate {

    public function handle($request, Closure $next, $guard = null) {

        if (Auth::guard($guard)->guest()):
            if ($request->ajax()):
                return response('Unauthorized.', 401);
            else:
                return redirect()->guest('login');
            endif;
        elseif (Auth::user()->group->dashboard == 'admin'):
            return $next($request);
        else:
            return redirect()->to('/');
        endif;
    }
}