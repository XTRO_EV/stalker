<?php

namespace STALKER_CMS\Vendor\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken extends BaseVerifier {

    protected $except = [];

    public function handle($request, \Closure $next) {

        try {
            return $next($request);
        } catch (TokenMismatchException $e) {
            if ($request->wantsJson()):
                return \ResponseController::error(400)->json();
            else:
                throw new Exception(\Lang::get('root_lang::codes.400'), 400);
            endif;
        }
    }
}
