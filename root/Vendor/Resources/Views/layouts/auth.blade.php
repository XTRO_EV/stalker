<!DOCTYPE html>
<!--[if IE 9 ]>
<html class="ie9">
<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="locale" content="{{ \App::getLocale()  }}">
    <meta name="fallback_locale" content="{{ config('app.fallback_locale')  }}">
    {!! Html::style('core/css/vendor.css') !!}
    {!! Html::style('core/css/main.css') !!}
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
</head>
<body class="@yield('body-class')">
@yield('content')
@include('root_views::assets.old-browser')
{!! Html::script('core/js/vendor.js') !!}
@yield('scripts_before')
{!! Html::script('core/js/main.js') !!}
@yield('scripts_after')
</body>
</html>