<?php
namespace STALKER_CMS\Vendor\Facades;

use Illuminate\Support\Facades\Facade;

class PackagesController extends Facade {

    protected static function getFacadeAccessor() {

        return 'PackagesController';
    }
}