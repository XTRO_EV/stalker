<?php
namespace STALKER_CMS\Vendor\Facades;

use Illuminate\Support\Facades\Facade;

class RequestController extends Facade {

    protected static function getFacadeAccessor() {

        return 'RequestController';
    }
}