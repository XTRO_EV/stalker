<?php
namespace STALKER_CMS\Vendor\Facades;

use Illuminate\Support\Facades\Facade;

class ResponseController extends Facade {

    protected static function getFacadeAccessor() {

        return 'ResponseController';
    }
}