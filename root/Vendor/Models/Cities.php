<?php
namespace STALKER_CMS\Vendor\Models;

class Cities {

    public static $cities = [
        'ru' => [
            'moscow' => 'Москва',
            'rostov-on-don' => 'Ростов-на-Дону'
        ],
        'en' => [
            'moscow' => 'Moscow',
            'rostov-on-don' => 'Rostov-on-Don'
        ],
        'es' => [
            'moscow' => 'Moscú',
            'rostov-on-don' => 'Rostov-on-Don'
        ]
    ];

    public static function getCities() {

        return self::$cities[\App::getLocale()];
    }
}