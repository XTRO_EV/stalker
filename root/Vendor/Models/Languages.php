<?php
namespace STALKER_CMS\Vendor\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;

class Languages extends BaseModel implements ModelInterface {

    public $timestamps = FALSE;

    protected $table = 'languages';
    protected $fillable = ['slug', 'title', 'iso_639_1', 'iso_639_2', 'iso_639_3', 'code', ' active', 'default', 'required'];
    protected $hidden = [];
    protected $guarded = ['id', '_method', '_token'];

    public $rules_store = ['slug' => 'required', 'title' => 'required', 'iso_639_1' => 'required', 'iso_639_2' => 'required', 'iso_639_3' => 'required', 'code' => 'required'];
    public $rules_update = ['slug' => 'required', 'title' => 'required', 'iso_639_1' => 'required', 'iso_639_2' => 'required', 'iso_639_3' => 'required', 'code' => 'required'];

    public function insert($request) {
        // TODO: Implement insert() method.
    }

    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    public function remove($id) {
        // TODO: Implement remove() method.
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }
}