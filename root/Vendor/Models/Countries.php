<?php
namespace STALKER_CMS\Vendor\Models;

class Countries {

    public static $countries = [
        'ru' => [
            'russia' => 'Российская Федерация',
            'ukraine' => 'Украина'
        ],
        'en' => [
            'russia' => 'Russian Federation',
            'ukraine' => 'Ukraine'
        ],
        'es' => [
            'russia' => 'Russian Federation',
            'ukraine' => 'Ucrania'
        ]
    ];

    public static function getCountries() {

        return self::$countries[\App::getLocale()];
    }
}