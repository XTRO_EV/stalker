<?php
namespace STALKER_CMS\Vendor\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;

class Packages extends BaseModel implements ModelInterface {

    public $timestamps = FALSE;

    protected $table = 'packages';
    protected $fillable = ['slug', 'title', 'description', 'install_path', 'enabled', 'required', 'relations', 'composer_config', 'order'];
    protected $hidden = [];
    protected $guarded = ['id', '_method', '_token'];

    public function insert($request) {

        $this->slug = $request['slug'];
        $this->title = $request['title'];
        $this->description = $request['description'];
        $this->composer_config = $request['composer_config'];
        $this->install_path = $request['install_path'];
        $this->enabled = $request['enabled'];
        $this->required = $request['required'];
        $this->relations = $request['relations'];
        $this->order = $request['order'];
        $this->save();
        return $this->id;
    }

    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    public function remove($id) {
        // TODO: Implement remove() method.
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {

        return self::orderBy('required', 'DESC')
            ->orderBy('order')
            ->whereEnabled(TRUE)
            ->where(function ($query) {
                $query->where('order', '>', 0);
                $query->where('order', '<', 9999);
            })
            ->get();
    }

    public function getTitleAttribute() {

        if (!empty($this->attributes['title'])):
            $json = json_decode($this->attributes['title'], TRUE);
            return isset($json[\App::getLocale()]) ? $json[\App::getLocale()] : '<i class="zmdi zmdi-mood-bad"></i>';
        endif;
    }

    public function getDescriptionAttribute() {

        if (!empty($this->attributes['description'])):
            $json = json_decode($this->attributes['description'], TRUE);
            return isset($json[\App::getLocale()]) ? $json[\App::getLocale()] : '<i class="zmdi zmdi-mood-bad"></i>';
        endif;
    }

    public static function getNestedPackages() {

        return self::orderBy('required', 'DESC')
            ->orderBy('order')
            ->whereEnabled(TRUE)
            ->where(function ($query) {
                $query->where('order', '>', 0);
                $query->where('order', '<', 9999);
            })
            ->get();
    }
}