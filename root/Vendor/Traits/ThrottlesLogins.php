<?php

namespace STALKER_CMS\Vendor\Traits;

use Illuminate\Http\Request;
use Illuminate\Cache\RateLimiter;
use Illuminate\Support\Facades\Lang;

trait ThrottlesLogins {

    protected function hasTooManyLoginAttempts($request) {

        return app(RateLimiter::class)->tooManyAttempts(
            $this->getThrottleKey($request),
            $this->maxLoginAttempts(), $this->lockoutTime() / 60
        );
    }

    protected function incrementLoginAttempts(\RequestController $request) {

        app(RateLimiter::class)->hit($this->getThrottleKey($request));
    }

    protected function retriesLeft(\RequestController $request) {

        $attempts = app(RateLimiter::class)->attempts($this->getThrottleKey($request));
        return $this->maxLoginAttempts() - $attempts + 1;
    }

    protected function sendLockoutResponse(\RequestController $request) {

        $seconds = app(RateLimiter::class)->availableIn($this->getThrottleKey($request));
        return \ResponseController::error(2403)->set('errorText', $this->getLockoutErrorMessage($seconds))->json();
    }

    protected function getLockoutErrorMessage($seconds) {

        return trans('core_auth_lang::auth.throttle', ['seconds' => $seconds]);
    }

    protected function clearLoginAttempts(\RequestController $request) {

        app(RateLimiter::class)->clear($this->getThrottleKey($request));
    }

    protected function getThrottleKey(\RequestController $request) {

        return mb_strtolower($request->input($this->loginUsername())) . '|' . $request->ip();
    }

    protected function maxLoginAttempts() {

        return property_exists($this, 'maxLoginAttempts') ? $this->maxLoginAttempts : 5;
    }

    protected function lockoutTime() {

        return property_exists($this, 'lockoutTime') ? $this->lockoutTime : 60;
    }
}
