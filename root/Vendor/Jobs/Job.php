<?php

namespace STALKER_CMS\Vendor\Jobs;

use Illuminate\Bus\Queueable;

abstract class Job {
    use Queueable;
}
