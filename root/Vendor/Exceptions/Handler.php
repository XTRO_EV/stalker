<?php

namespace STALKER_CMS\Vendor\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class
    ];

    public function report(Exception $e) {

        return parent::report($e);
    }

    public function render($request, Exception $e) {

//        print_r($e->getMessage());exit;
        $statusCode = $this->getStatusCode($e);
        if ($request->wantsJson()):
            if (config('app.debug') === FALSE):
                return \ResponseController::error($statusCode)->set('errorText', trans('root_lang::codes.' . $statusCode))->json();
            else:
                return parent::render($request, $e);
            endif;
        endif;
        return $this->convertExceptionToResponse($e);
        #return parent::render($request, $e);
    }

    protected function convertExceptionToResponse(Exception $e) {

        if (config('app.debug')):
            return parent::convertExceptionToResponse($e);
        else:
            $statusCode = $this->getStatusCode($e);
            if (view()->exists("site_views::errors.$statusCode")):
                return response()->view(
                    "site_views::errors.$statusCode",
                    ['code' => $statusCode, 'message' => trans('root_lang::codes.' . $statusCode)],
                    $statusCode <= 1000 ? $statusCode : 500);
            endif;
            if (view()->exists("root_views::errors.$statusCode")):
                return response()->view(
                    "root_views::errors.$statusCode",
                    ['code' => $statusCode, 'message' => trans('root_lang::codes.' . $statusCode)],
                    $statusCode <= 1000 ? $statusCode : 500);
            endif;
        endif;
    }

    protected function getStatusCode(Exception $e) {

        if (file_exists(base_path('.env')) === FALSE):
            return 2005;
        elseif ($e instanceof ModelNotFoundException):
            return 404;
        elseif ($e instanceof NotFoundHttpException):
            return 404;
        elseif ($e instanceof \InvalidArgumentException):
            return 404;
        elseif ($e instanceof MethodNotAllowedHttpException):
            return 405;
        elseif ($e instanceof HttpException):
            return $e->getStatusCode();
        elseif ($e->getCode() > 0):
            return $e->getCode();
        else:
            return 500;
        endif;
    }
}
