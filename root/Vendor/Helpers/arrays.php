<?php
namespace STALKER_CMS\Vendor\Helpers;

/**
 * Возвращает текст по ключу массива исходя из текущей локали
 * @param array $trans
 * @return mixed
 */
function array_translate(array $trans) {

    return array_first($trans, function ($key, $value) {
        return $key == \App::getLocale();
    });
}

function recursive(&$rs, $parent) {

    $out = [];
    if (!isset($rs[$parent])):
        return $out;
    endif;
    foreach ($rs[$parent] as $row):
        $chidls = recursive($rs, $row['id']);
        if ($chidls):
            $row['sub_menu'] = $chidls;
        endif;
        $out[] = $row;
    endforeach;
    return $out;
}